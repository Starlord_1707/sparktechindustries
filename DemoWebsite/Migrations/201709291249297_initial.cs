namespace DemoWebsite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CartDetails",
                c => new
                    {
                        CartDetailID = c.Int(nullable: false, identity: true),
                        CartMasterID = c.Int(nullable: false),
                        ProductID = c.Int(nullable: false),
                        ProductColor = c.String(),
                        ProductSize = c.String(),
                        UnitPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ProductQty = c.Int(nullable: false),
                        Status = c.String(),
                    })
                .PrimaryKey(t => t.CartDetailID);
            
            CreateTable(
                "dbo.CartMasters",
                c => new
                    {
                        CartMasterID = c.Int(nullable: false, identity: true),
                        UserID = c.Int(nullable: false),
                        Status = c.String(),
                        OrderID = c.Int(),
                        OrderPrice = c.Decimal(precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.CartMasterID);
            
            CreateTable(
                "dbo.ContactUs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 150),
                        Email = c.String(nullable: false, maxLength: 150),
                        Message = c.String(nullable: false, maxLength: 2500),
                        CreatedDate = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Feedbacks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductID = c.Int(nullable: false),
                        UserID = c.Int(nullable: false),
                        Email = c.String(),
                        Content = c.String(),
                        Status = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.LoginHistories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                        LoginTime = c.DateTime(nullable: false),
                        LogoutTime = c.DateTime(),
                        UserIPAddress = c.String(),
                        UserOS = c.String(),
                        UserType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Logins",
                c => new
                    {
                        emailID = c.String(nullable: false, maxLength: 128),
                        password = c.String(),
                        guid = c.String(),
                    })
                .PrimaryKey(t => t.emailID);
            
            CreateTable(
                "dbo.OrderDetails",
                c => new
                    {
                        OrderDetailsID = c.Int(nullable: false, identity: true),
                        OrderID = c.Int(nullable: false),
                        ProductId = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        Status = c.String(),
                    })
                .PrimaryKey(t => t.OrderDetailsID);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        OrderID = c.Int(nullable: false, identity: true),
                        OrderNumber = c.String(),
                        UserID = c.Int(nullable: false),
                        OrderAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Invoice = c.String(),
                        OrderDate = c.DateTime(),
                        DeliveredDate = c.DateTime(),
                        Status = c.String(),
                    })
                .PrimaryKey(t => t.OrderID);
            
            CreateTable(
                "dbo.ProductDetails",
                c => new
                    {
                        ProductID = c.Int(nullable: false, identity: true),
                        ProductName = c.String(),
                        Description = c.String(),
                        ImagePath = c.String(),
                        ModelNumber = c.String(),
                        Capacity = c.String(),
                        Weight = c.String(),
                        DimensionApprox = c.String(),
                        PowerSource = c.String(),
                        Chassis = c.String(),
                        Mobility = c.String(),
                        HppCylinders = c.String(),
                        Availability = c.String(),
                        UnitPrice = c.Double(),
                        CategoryID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ProductID);
            
            CreateTable(
                "dbo.ProductImages",
                c => new
                    {
                        imageID = c.Int(nullable: false, identity: true),
                        productID = c.Int(nullable: false),
                        productThumbnail = c.String(),
                        productFullSize = c.String(),
                        imageDescription = c.String(),
                        Status = c.String(),
                    })
                .PrimaryKey(t => t.imageID);
            
            CreateTable(
                "dbo.ProductsCategories",
                c => new
                    {
                        CategoryID = c.Int(nullable: false, identity: true),
                        CategoryName = c.String(),
                        ParentID = c.Int(nullable: false),
                        Status = c.String(),
                    })
                .PrimaryKey(t => t.CategoryID);
            
            CreateTable(
                "dbo.ShoppingCartItems",
                c => new
                    {
                        ItemId = c.String(nullable: false, maxLength: 128),
                        CartId = c.String(),
                        Quantity = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        ProductId = c.Int(nullable: false),
                        TotalCartItems = c.Int(nullable: false),
                        Status = c.String(),
                    })
                .PrimaryKey(t => t.ItemId);
            
            CreateTable(
                "dbo.UserRegistrations",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Email = c.String(),
                        PhoneNumber = c.String(),
                        Password = c.String(),
                        DOB = c.String(),
                        Gender = c.String(),
                        Street = c.String(),
                        City = c.String(),
                        State = c.String(),
                        Country = c.String(),
                        PinCode = c.String(),
                        Role = c.String(),
                        Status = c.String(),
                        RememberMe = c.Boolean(nullable: false),
                        LastLogin = c.DateTime(),
                    })
                .PrimaryKey(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.UserRegistrations");
            DropTable("dbo.ShoppingCartItems");
            DropTable("dbo.ProductsCategories");
            DropTable("dbo.ProductImages");
            DropTable("dbo.ProductDetails");
            DropTable("dbo.Orders");
            DropTable("dbo.OrderDetails");
            DropTable("dbo.Logins");
            DropTable("dbo.LoginHistories");
            DropTable("dbo.Feedbacks");
            DropTable("dbo.ContactUs");
            DropTable("dbo.CartMasters");
            DropTable("dbo.CartDetails");
        }
    }
}
