﻿using DemoWebsite.Models;
using DemoWebsite.Models.DBContext;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace DemoWebsite.Services
{
    public interface IOrderService
    {
        bool SaveOrder(Orders viewmodel);
        Orders GetDataByUserIdAndOrderNumber(int id,string orderNo);
        List<OrderDetails> GetOrderDetailDataByOrderId(int id);
        bool SaveOrderDetails(OrderDetails viewmodel);
    }
    public class OrderService : IOrderService
    {
        private SparkTechDBContext db = new SparkTechDBContext();
        
        public OrderService(SparkTechDBContext tDB)
        {
            if (tDB == null)
            {
                throw new ArgumentNullException("tDB");
            }
            this.db = tDB;
        }
        public bool SaveOrder(Orders viewmodel)
        {
            try
            {
                var modelData = db.Orders.FirstOrDefault(a=> a.OrderID == viewmodel.OrderID);
                if (viewmodel.OrderID != 0)
                {                    
                    viewmodel.Status = "Processing";
                }
               
                if (modelData == null)
                {

                    db.Orders.Add(viewmodel);
                    db.SaveChanges();
                    
                }
                else
                    db.SaveChanges();

                return true;
            }
            catch
            {
                return false;
            }
        }
        public Orders GetDataByUserIdAndOrderNumber(int id, string orderNo)
        {
            try
            {
                return db.Orders.OrderByDescending(a=> a.OrderDate).FirstOrDefault(a => a.UserID == id && a.OrderNumber == orderNo);
            }
            catch
            {
                return null;
            }
        }
        public bool SaveOrderDetails(OrderDetails viewmodel)
        {
            try
            {
                var modelData = db.OrderDetails.FirstOrDefault(a => a.OrderDetailsID == viewmodel.OrderDetailsID);
                if (modelData==null)
                {
                    db.OrderDetails.Add(viewmodel);
                    db.SaveChanges();
                }                    
                else
                    db.SaveChanges();

                return true;
            }
            catch
            {
                return false;
            }
        }
        public  List<OrderDetails> GetOrderDetailDataByOrderId(int id)
        {
            try
            {
                var modelData = db.OrderDetails.Where(a => a.OrderID == id).ToList();
                return modelData;
            }
            catch
            {
                return null;
            }
        }
    }
}