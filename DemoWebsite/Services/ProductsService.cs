﻿using DemoWebsite.Models;
using DemoWebsite.Models.DBContext;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace DemoWebsite.Services
{
    public interface IProductsService
    {
        List<ProductsCategory> findMainCategory();  // Temp Method for Main Category Only
        List<int> findAllCategory(int id);
        ProductDetails GetSingleProductDetailByCategoryID(int id);
        List<ProductDetails> GetProductDetailByCategoryID(int id);
        ProductsCategory GetCategoryDetail(int id);
        ProductDetails GetSingleProductDetailByProductID(int id);
        List<ProductDetails> GetAllProductDetail();        

    }
    public class ProductsService : IProductsService
    {
        private SparkTechDBContext db = new SparkTechDBContext();
        public ProductsService(SparkTechDBContext tDB)
        {
            if (tDB == null)
            {
                throw new ArgumentNullException("tDB");
            }
            this.db = tDB;
        }


        public List<ProductsCategory> findMainCategory()
        {
            try
            {
                return db.ProductsCategory.Where(v => v.Status != "Delete").ToList();
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public List<int> findAllCategory(int id)
        {
            var allCategoryData = db.ProductsCategory.ToList();
            var tempVar = new List<int>();
            foreach (var v in allCategoryData)
            {
                if (v.ParentID == id)
                {
                    if (v.Status != "Delete")
                    {

                        List<int> tempListAdd = findAllCategory(v.CategoryID);
                        tempVar.Add(v.CategoryID);
                        tempVar.AddRange(tempListAdd);
                    }
                }

            }
            return tempVar;
        }
        public ProductDetails GetSingleProductDetailByCategoryID(int id)
        {
            return db.ProductDetails.FirstOrDefault(a => a.CategoryID == id);
        }
        public List<ProductDetails> GetProductDetailByCategoryID(int id)
        {
            return db.ProductDetails.Where(a => a.CategoryID == id).ToList();
        }
        public ProductsCategory GetCategoryDetail(int id)
        {
            return db.ProductsCategory.FirstOrDefault(a => a.CategoryID == id);
        }        
        public ProductDetails GetSingleProductDetailByProductID(int id)
        {
            return db.ProductDetails.FirstOrDefault(a => a.ProductID == id);
        }
        public List<ProductDetails> GetAllProductDetail()
        {
            return db.ProductDetails.Where(a=> a.Availability == "Y").ToList();
        }
    }
}