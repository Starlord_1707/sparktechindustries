﻿using DemoWebsite.Models;
using DemoWebsite.Models.DBContext;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace DemoWebsite.Services
{
    public interface IUserService
    {
        Task<Responses<bool>> SaveData(UserRegistration viewmodel);
        Responses<UserRegistration> CheckLogin(UserRegistration model);
        UserRegistration GetUserDataByUserID(int id);
        Responses<bool> SaveDataDuringCheckout(UserRegistration viewmodel);

    }
    public class UserService : IUserService
    {
        private readonly SparkTechDBContext db;

        public UserService(SparkTechDBContext tDB)
        {
            if (tDB == null)
            {
                throw new ArgumentNullException("tDB");
            }
            this.db = tDB;
        }
        public Task<Responses<bool>> SaveData(UserRegistration model)
        {
            UserRegistration obj = null;
            return Task.Run(
                 async () =>
                 {
                     var exceptions = new Dictionary<string, string>();
                     var response = new Responses<bool>();
                     try
                     {                         
                         obj = db.UserRegistrations.Where(u => u.Email.ToLower().Equals(model.Email.ToLower().Trim())
                                     && u.Status != CryptoUtilities.Delete && u.PhoneNumber == model.PhoneNumber).FirstOrDefault();

                         if (obj == null)
                         {
                             
                             if (model.UserId == 0)
                             {
                                 if (model.FirstName != null)
                                 {
                                     if (model.LastName != null)
                                         db.UserRegistrations.Add(model); // Later Change this to Repository Pattern
                                     else
                                     {
                                         exceptions.Add("FieldNull", "Last Name should not be Empty");
                                         response.Response = false;
                                     }

                                 }
                                 else
                                 {
                                     exceptions.Add("FieldNull", "First Name should not be Empty");
                                     response.Response = false;
                                 }

                             }
                             else
                             {
                                 // Later Change this to Repository Pattern
                                 var original = db.UserRegistrations.FirstOrDefault(u => u.UserId.Equals(model.UserId));
                                 if (original != null)
                                 {
                                     db.Entry(original).CurrentValues.SetValues(model);
                                 }                                 
                             }
                             var recordaffected = await db.SaveChangesAsync();
                             if (recordaffected > 0)
                             {
                                 response.Response = true;
                                 response.Exceptions = exceptions;
                             }
                         }
                         else
                         {
                             exceptions.Add("FieldSame", "Phone Number/Email already exist");
                             response.Response = false;
                         }

                     }
                     catch (SqlException sqlexception)
                     {
                         exceptions.Add("SqlException", sqlexception.Message);
                         response.Response = false;
                     }
                     response.Exceptions = exceptions;
                     return response;
                 });
        }
        public Responses<UserRegistration> CheckLogin(UserRegistration model)
        {
            UserRegistration objiUser = null;
            var exceptions = new Dictionary<string, string>();            
            try
            {
                var user = db.UserRegistrations.Where(o => (o.Status != CryptoUtilities.Delete) && (o.Email.Equals(model.Email))).FirstOrDefault();                
                if (user != null)
                {
                    if (model.Password.Equals((user.Password)))
                    {
                        objiUser = user;
                    }                
                    else
                    {
                        exceptions.Add("ValidUser", "Password is incorrect!");
                        return new Responses<UserRegistration>
                        {
                            Exceptions = exceptions,
                            Response = objiUser
                        };
                    }
                }
                else
                {
                    exceptions.Add("InValidUser", "User name is not available!");
                    return new Responses<UserRegistration>
                    {
                        Exceptions = exceptions,
                        Response = objiUser
                    };
                }

            }
            catch (SqlException sqlException)
            {
                exceptions.Add("SqlException", sqlException.Message);
            }
            catch (TaskCanceledException taskCanceledException)
            {
                exceptions.Add("TaskCanceledException", taskCanceledException.Message);
            }
            return new Responses<UserRegistration>
            {
                Exceptions = exceptions,
                Response = objiUser
            };            
        }
        public UserRegistration GetUserDataByUserID(int id)
        {
            return db.UserRegistrations.FirstOrDefault(a => a.UserId == id);
        }
        public Responses<bool> SaveDataDuringCheckout(UserRegistration viewmodel)
        {
            var response = new Responses<bool>();
            try
            {
                var modelData = GetUserDataByUserID(viewmodel.UserId);
                if (viewmodel.UserId != 0)
                {
                    viewmodel.LastName = modelData.LastName;
                    viewmodel.DOB = modelData.DOB;
                    viewmodel.Gender = modelData.Gender;
                    viewmodel.LastLogin = modelData.LastLogin;
                    viewmodel.Password = modelData.Password;                    
                    viewmodel.Email = modelData.Email;
                    viewmodel.PhoneNumber = modelData.PhoneNumber;
                    viewmodel.Role = modelData.Role;
                    viewmodel.Status = modelData.Status;

                }

                if (viewmodel.UserId == 0)
                    response.Response = false;
                else
                {
                    var original = db.UserRegistrations.FirstOrDefault(u => u.UserId.Equals(viewmodel.UserId));
                    if (original != null)
                    {
                        db.Entry(original).CurrentValues.SetValues(viewmodel);
                    }
                    response.Response = true;
                    db.SaveChanges();
                }                    
            }
            catch (SqlException sqlexception)
            {               
                response.Response = false;
            }

            return response;
        
        }

        //public void InsertLoginHistory(string userId, CommonEnums.UserType userType)
        //{

        //    var logoutTimeCheck = db.LoginHistory.FirstOrDefault(a => a.LogoutTime == null && a.UserId == userId);
        //    if (logoutTimeCheck != null)
        //    {
        //        logoutTimeCheck.LogoutTime = DateTime.Now;                
        //        db.LoginHistory.Update(logoutTimeCheck);
        //        unitOfWork.Save();
        //    }

        //    unitOfWork.LoginHistoryRepository.Insert(new LoginHistory
        //    {
        //        LoginTime = DateTime.Now,
        //        UserId = userId,
        //        UserType = userType,
        //        UserOS = GetOsName(),
        //        UserIPAddress = GetIpAddress()

        //    });
        //    unitOfWork.Save();
        //}
    }
}