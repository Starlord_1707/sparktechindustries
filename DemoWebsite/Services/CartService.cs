﻿using DemoWebsite.Models;
using DemoWebsite.Models.DBContext;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace DemoWebsite.Services
{
    public interface ICartService
    {
        CartMaster GetDataByUserId(int id);
        int CountbyCartMasterId(int id);
        bool SaveCartMasterData(CartMaster viewmodel);
        bool SaveCartDetailsData(CartDetails viewmodel);
        IEnumerable<CartDetails> GetDataByCartMasterId(int id);
        CartDetails GetCartDetailDataById(int id);
        string GetCartId();
        List<ShoppingCartItems> GetCartItems();
        ShoppingCartItems GetSingleCartItems(string cartID, int productID);
        bool SaveShoppingCart(ShoppingCartItems viewmodel);
        bool UpdateShoppingCart(ShoppingCartItems viewmodel);
        List<ShoppingCartItems> GetCartItemsByEmailID(string email);

        void updateCartDataInDb(string emailId);
        bool DeleteCartItemUsingProductId(int id,string emailId);

    }
    public class CartService : ICartService
    {
        private SparkTechDBContext db = new SparkTechDBContext();
        public string ShoppingCartId { get; set; }

        public const string CartSessionKey = "CartId";

        public CartService(SparkTechDBContext tDB)
        {
            if (tDB == null)
            {
                throw new ArgumentNullException("tDB");
            }
            this.db = tDB;
        }        
        public CartMaster GetDataByUserId(int id)
        {            
            try
            {
                var module = db.CartMasters.FirstOrDefault(a=> a.UserID == id && a.Status=="Pending");                
                return module;
            }
            catch 
            {                
                return null;
            }
        }

        public bool SaveCartMasterData(CartMaster viewmodel)
        {
            bool result;
            try
            {
                var modelData = db.CartMasters.FirstOrDefault(a=> a.CartMasterID == viewmodel.CartMasterID);
                if (viewmodel.CartMasterID != 0)
                {
                    
                    if (viewmodel.Status == null)
                    {
                        viewmodel.Status = "Pending";
                    }
                    if (viewmodel.OrderID != null)
                    {
                        viewmodel.Status = "Processing";
                    }
                    else if (viewmodel.OrderPrice != null)
                    {
                        viewmodel.Status = "Pending";
                    
                    }

                }
                else if (viewmodel.CartMasterID == 0)
                {                    
                    viewmodel.Status = "Pending";

                }
                

                if (viewmodel.CartMasterID == 0)
                {
                    db.CartMasters.Add(viewmodel);
                    db.SaveChanges();
                    result =  true;                    
                }
                else if (viewmodel.OrderID != null)
                {
                    var original = db.CartMasters.FirstOrDefault(u => u.CartMasterID.Equals(viewmodel.CartMasterID));
                    if (original != null)
                    {
                        db.Entry(original).CurrentValues.SetValues(viewmodel);
                    }
                    db.SaveChanges();
                }
                else if (viewmodel.OrderPrice != null)
                {
                    var original = db.CartMasters.FirstOrDefault(u => u.CartMasterID.Equals(viewmodel.CartMasterID));
                    if (original != null)
                    {
                        db.Entry(original).CurrentValues.SetValues(viewmodel);
                    }
                }
                else if (viewmodel.OrderPrice == null && viewmodel.CartMasterID != 0)
                {
                    var original = db.CartMasters.FirstOrDefault(u => u.CartMasterID.Equals(viewmodel.CartMasterID));
                    if (original != null)
                    {
                        db.Entry(original).CurrentValues.SetValues(viewmodel);
                    }
                }               
                result = true;
                db.SaveChanges();
            }
            catch
            {

                result = false;
            }
            return result;
        }

        public bool SaveCartDetailsData(CartDetails viewmodel)
        {
            bool result;
            try
            {
                var modelData = db.CartDetails.FirstOrDefault(a => a.CartDetailID == viewmodel.CartDetailID);

                if (viewmodel.CartMasterID == 0)
                {
                    viewmodel.Status = "Pending";

                }


                if (viewmodel.CartDetailID == 0)
                {
                    db.CartDetails.Add(viewmodel);
                    result = true;
                }
                else
                {
                    var original = db.CartDetails.FirstOrDefault(u => u.CartDetailID.Equals(viewmodel.CartDetailID));
                    if (original != null)
                    {
                        db.Entry(original).CurrentValues.SetValues(viewmodel);
                    }
                    result = true;
                }
                db.SaveChanges();
            }
            catch
            {

                result = false;
            }
            return result;
        }

        public int CountbyCartMasterId(int id)
        {          
            int recCount = 0;
            if (id != 0)
            {
                recCount = db.CartDetails.Where(c => c.CartMasterID.Equals(id) && c.Status != "Delete" && c.Status != "Approve").Count();
            }
            return recCount;
            
        }

        public IEnumerable<CartDetails> GetDataByCartMasterId(int id)
        {
           
            IEnumerable<CartDetails> moduleviewmodel = null;
            try
            {
                var module = db.CartDetails.Where(a=> a.CartMasterID==id && a.Status!="Delete" ).ToList();
                if (module != null)
                {
                    moduleviewmodel = module;
                }
                return module;
            }
            catch
            {
                return null;
            }
        }

        public CartDetails GetCartDetailDataById(int id)
        {
            return db.CartDetails.FirstOrDefault(a=> a.CartDetailID==id && a.Status != "Delete");
        }
        public string GetCartId()
        {
            if (System.Web.HttpContext.Current.Session[CartSessionKey] == null)
            {
                if (!string.IsNullOrWhiteSpace(System.Web.HttpContext.Current.User.Identity.Name))
                {
                    System.Web.HttpContext.Current.Session[CartSessionKey] = System.Web.HttpContext.Current.User.Identity.Name;
                }
                else
                {
                    // Generate a new random GUID using System.Guid class.     
                    //Guid tempCartId = Guid.NewGuid();
                    //System.Web.HttpContext.Current.Session[CartSessionKey] = tempCartId.ToString();
                    System.Web.HttpContext.Current.Session[CartSessionKey] = System.Web.HttpContext.Current.Session.SessionID;
                }
            }
            return System.Web.HttpContext.Current.Session[CartSessionKey].ToString();
        }

        public List<ShoppingCartItems> GetCartItems()
        {
            ShoppingCartId = GetCartId();
            return db.ShoppingCartItems.Where(c => c.CartId == ShoppingCartId && c.Status == "Pending").ToList()??null;
        }
        public List<ShoppingCartItems> GetCartItemsByEmailID(string email)
        {
            
            return db.ShoppingCartItems.Where(c => c.CartId == email && c.Status == "Pending").ToList();
        }
        public ShoppingCartItems GetSingleCartItems(string cartID,int productID)
        {
            return db.ShoppingCartItems.FirstOrDefault( c => c.CartId == cartID && c.ProductId == productID && c.Status=="Pending");
        }
        public bool SaveShoppingCart(ShoppingCartItems viewmodel)
        {
            try
            {
                db.ShoppingCartItems.Add(viewmodel);
                UpdateShoppingCart(viewmodel);
                return true;
            }
            catch
            {
                return false;
            }
            
        }

        public bool UpdateShoppingCart(ShoppingCartItems viewmodel)
        {
            try
            {
                db.SaveChanges();                
                return true;
            }
            catch
            {
                return false;
            }

        }
        
        public void updateCartDataInDb(string emailId)
        {
            if (System.Web.HttpContext.Current.Session["CartId"] != null)
            {
                string sessionId = System.Web.HttpContext.Current.Session.SessionID;
                var list = db.ShoppingCartItems.Where(c => c.CartId == sessionId).ToList();
                if (list != null)
                {                    
                    foreach (var item in list)
                    {
                        var checkAlreadyExistItem = db.ShoppingCartItems.Where(a => a.CartId == emailId && a.ProductId == item.ProductId && a.Status == "Pending").FirstOrDefault();
                        if(checkAlreadyExistItem!=null)
                        {
                            checkAlreadyExistItem.Quantity = checkAlreadyExistItem.Quantity + item.Quantity;
                            item.Status = "Delete";
                        }
                        else
                        {
                            item.CartId = emailId;
                        }
                        db.SaveChanges();
                    }
                    
                    #region -commented due to some exceptions
                    //List<ShoppingCartItems> listOfShoppingCart = db.ShoppingCartItems.GroupBy(x => x.ProductId, (Key, groupedItems) => new ShoppingCartItems
                    //{
                    //    ProductId = Key,
                    //    CartId = groupedItems.Select(m => m.CartId).FirstOrDefault(),
                    //    DateCreated = groupedItems.Select(m => m.DateCreated).FirstOrDefault(),
                    //    ItemId = groupedItems.Select(m => m.ItemId).FirstOrDefault(),
                    //    Quantity = groupedItems.Select(m => m.Quantity).Sum(),
                    //    TotalCartItems = groupedItems.Select(m => m.TotalCartItems).FirstOrDefault()
                    //}).ToList();
                    //db.ShoppingCartItems.AddRange(listOfShoppingCart);
                    //db.SaveChanges();
                    #endregion
                    System.Web.HttpContext.Current.Session["CartId"] = emailId;
                }
            }            
        }
        public bool DeleteCartItemUsingProductId(int id, string emailId)
        {
            try
            {
                var cartDetails = GetSingleCartItems(emailId,id);

                cartDetails.Status = "Delete";
                UpdateShoppingCart(cartDetails);
                                
                return true;
            }
            catch
            {
                return false;
            }
        }

    }
}