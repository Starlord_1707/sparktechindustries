﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace DemoWebsite.Services
{
    public class EmailTriggerService
    {
        public enum EmailBodyType
        {
            CreateUser,
            ForgotPassword,
            VerifyCredentials,
            MessageFromUser
        }
        public static string SendEmailToUser(string toEmail, string emailBody, string emailSubject, bool emailBodyIsHtml)
        {
            var message = new MailMessage();
            message.To.Add(new MailAddress(toEmail));//replace with valid value
            message.Subject = emailSubject;
            message.Body = emailBody;
            message.IsBodyHtml = emailBodyIsHtml;
            var returnMessage = "Unsuccessful";
            using (var smtp = new SmtpClient())
                try
                {
                    smtp.Send(message);
                    //smtp.Dispose();
                    returnMessage = "Email has been sent successfully";
                }
                catch (Exception ex)
                {
                    var val = ex;
                }
            return returnMessage;
        }

        public string SendEmailDataContent(string userName, string userEmail, string toEmail, string bodyContent, EmailBodyType emailBodyType)
        {
            var emailBodyIsHtml = true;
            var body = string.Empty;
            var emailSubject = string.Empty;
            StreamReader reader;
            string readFile = string.Empty;
            string myString = string.Empty;
            //get html template according type of email body
            switch (emailBodyType)
            {
                case EmailBodyType.MessageFromUser:
                    reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Views/Shared/EmailBodyContent/EmailToSupportTeam.cshtml"));
                    readFile = reader.ReadToEnd();
                    myString = "";
                    myString = readFile;
                    myString = myString.Replace("{BodyContent}", bodyContent);
                    myString = myString.Replace("{UserName}", userName);
                    myString = myString.Replace("{Email}", userEmail);
                    body = myString.ToString();
                    emailSubject = "Message From User";
                    break;

                case EmailBodyType.VerifyCredentials:
                    reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Views/Shared/EmailBodyContent/VerifyCredentials.cshtml"));
                    readFile = reader.ReadToEnd();
                    myString = "";
                    myString = readFile;
                    myString = myString.Replace("{BodyContent}", bodyContent);
                    myString = myString.Replace("{UserName}", userName);
                    body = myString.ToString();
                    emailSubject = "Spark-Tech - " + bodyContent + " is your verification code";
                    break;

                //case EmailBodyType.CreateUser:
                //    StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Views/Shared/EmailBodyContent/CreateUser.cshtml"));
                //    string readFile = reader.ReadToEnd();
                //    string myString = "";
                //    myString = readFile;
                //    myString = myString.Replace("{UserName}", userName);
                //    myString = myString.Replace("{Password}", passwordToSend);
                //    body = myString.ToString();
                //    emailSubject = "Credentials for login.";
                //    break;

                //case EmailBodyType.FogotPassword:
                //    StreamReader reader1 = new StreamReader(HttpContext.Current.Server.MapPath("~/Views/Shared/EmailBodyContent/ForgotPassword.cshtml"));
                //    //StreamReader reader1 = new StreamReader((Path.Combine(HttpRuntime.AppDomainAppPath, "Views\\Shared\\EmailBodyContent\\ForgotPassword.cshtml")).Replace('\\','/'));
                //    string readFile1 = reader1.ReadToEnd();
                //    string myString1 = "";
                //    myString1 = readFile1;
                //    myString1 = myString1.Replace("{UserName}", userName);
                //    myString1 = myString1.Replace("{Password}", passwordToSend);
                //    body = myString1.ToString();
                //    emailSubject = "Credentials for login.";
                //    break;
                default:
                    // ViewBag.Message = "There was an unknown error.";
                    break;
            }
            var displayText = SendEmailToUser(toEmail, body, emailSubject, emailBodyIsHtml);
            return displayText;
        }

        //public static void SendEmailData(string userName, string userEmail, string toEmail, EmailBodyType emailBodyType)
        //{
        //    var emailBodyIsHtml = true;
        //    var body = string.Empty;
        //    var emailSubject = string.Empty;


        //    switch (emailBodyType)
        //    {
        //        case EmailBodyType.MessageFromUser:
        //            StreamReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/Views/Shared/EmailBodyContent/EmailToSupportTeam.cshtml"));
        //            string readFile = reader.ReadToEnd();
        //            string myString = "";
        //            myString = readFile;
        //            myString = myString.Replace("{UserName}", userName);
        //            myString = myString.Replace("{Email}", userEmail);
        //            body = myString.ToString();
        //            emailSubject = "Message From User";
        //            break;

        //        //case EmailBodyType.CreateUser:
        //        //    StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Views/Shared/EmailBodyContent/CreateUser.cshtml"));
        //        //    string readFile = reader.ReadToEnd();
        //        //    string myString = "";
        //        //    myString = readFile;
        //        //    myString = myString.Replace("{UserName}", userName);
        //        //    myString = myString.Replace("{Password}", passwordToSend);
        //        //    body = myString.ToString();
        //        //    emailSubject = "Credentials for login.";
        //        //    break;

        //        //case EmailBodyType.FogotPassword:
        //        //    StreamReader reader1 = new StreamReader(HttpContext.Current.Server.MapPath("~/Views/Shared/EmailBodyContent/ForgotPassword.cshtml"));
        //        //    //StreamReader reader1 = new StreamReader((Path.Combine(HttpRuntime.AppDomainAppPath, "Views\\Shared\\EmailBodyContent\\ForgotPassword.cshtml")).Replace('\\','/'));
        //        //    string readFile1 = reader1.ReadToEnd();
        //        //    string myString1 = "";
        //        //    myString1 = readFile1;
        //        //    myString1 = myString1.Replace("{UserName}", userName);
        //        //    myString1 = myString1.Replace("{Password}", passwordToSend);
        //        //    body = myString1.ToString();
        //        //    emailSubject = "Credentials for login.";
        //        //    break;
        //        default:
        //            // ViewBag.Message = "There was an unknown error.";
        //            break;
        //    }
        //    var displayText = SendEmailToUser(toEmail, body, emailSubject, emailBodyIsHtml);
        //}

    }
}