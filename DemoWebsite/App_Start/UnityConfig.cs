using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc5;
using DemoWebsite.Services;

namespace DemoWebsite
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType<IUserService, UserService>();
            container.RegisterType<IProductsService, ProductsService>();
            container.RegisterType<ICartService, CartService>();
            container.RegisterType<IOrderService, OrderService>();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}