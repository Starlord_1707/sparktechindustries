﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace DemoWebsite
{
    public class SessionUtilities
    {
        #region Actions
        public static string GetAuthenticatedName()
        {
            ClaimsIdentity identity = (ClaimsIdentity)HttpContext.Current.User.Identity;
            var authenticatedClientId = identity.FindFirst("Name");

            if (authenticatedClientId == null)
                return string.Empty;

            return Convert.ToString(authenticatedClientId.Value);
        }

        public static string GetAuthenticatedUserType()
        {
            ClaimsIdentity identity = (ClaimsIdentity)HttpContext.Current.User.Identity;
            var authenticatedClientId = identity.FindFirst("Role");

            if (authenticatedClientId == null)
                return string.Empty;

            return Convert.ToString(authenticatedClientId.Value);
        }
        public static string GetAuthenticatedUserEmail()
        {
            ClaimsIdentity identity = (ClaimsIdentity)HttpContext.Current.User.Identity;
            var authenticatedClientId = identity.FindFirst("Email");

            if (authenticatedClientId == null)
                return string.Empty;

            return (authenticatedClientId.Value);
        }
        public static int GetAuthenticatedUserId()
        {
            if (HttpContext.Current != null)
            {
                ClaimsIdentity identity = (ClaimsIdentity)HttpContext.Current.User.Identity;
                var authenticatedClientId = identity.Claims.ToList();
                if (authenticatedClientId.FirstOrDefault().Value=="" || authenticatedClientId.FirstOrDefault().Value == null)
                {
                    return 0;
                }
                else
                {
                    var value = authenticatedClientId.FirstOrDefault(e => e.Value != null).Value;
                    return Convert.ToInt32(value);
                }               
            }
            return 0;
        }




        #endregion
    }
}