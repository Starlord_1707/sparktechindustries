﻿using DemoWebsite.Models.DBContext;
using DemoWebsite.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoWebsite.UnitOfWork
{
    public interface IDemoWebsiteUnitOfWork
    {
        IUserService UserService { get; }
        IProductsService ProductsService { get; }
        ICartService CartService { get; }
        IOrderService OrderService { get; }
    }

    public class DemoWebsiteUnitOfWork : IDemoWebsiteUnitOfWork
    {
        private SparkTechDBContext db;
        public DemoWebsiteUnitOfWork(SparkTechDBContext sparkTechDBContext)
        {
            db = sparkTechDBContext;
        }

        // Add services here

        private IUserService userService;

        public IUserService UserService
        {
            get
            {
                if (userService == null)
                {
                    userService = new UserService(db);
                }
                return userService;
            }
        }

        private ICartService cartService;

        public ICartService CartService
        {
            get
            {
                if (cartService == null)
                {
                    cartService = new CartService(db);
                }
                return cartService;
            }
        }

        private IProductsService productsService;

        public IProductsService ProductsService
        {
            get
            {
                if (productsService == null)
                {
                    productsService = new ProductsService(db);
                }
                return productsService;
            }
        }

        private IOrderService orderService;

        public IOrderService OrderService
        {
            get
            {
                if (orderService == null)
                {
                    orderService = new OrderService(db);
                }
                return orderService;
            }
        }



        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}