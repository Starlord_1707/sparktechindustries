﻿using DemoWebsite.Models;
using DemoWebsite.Models.DBContext;
using DemoWebsite.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace DemoWebsite.Controllers
{
    public class ProductsController : Controller
    {

        private readonly ICartService cartService;
        private readonly IProductsService productService;
        private readonly IUserService userService;


        public ProductsController(IProductsService prodServ, ICartService cartServ, IUserService userSer)
        {
            productService = prodServ;
            cartService = cartServ;
            userService = userSer;
        }

        // GET: Products
        public ActionResult Index()
        {
            #region Implement Later
            //UltimateViewModel ultimate = new UltimateViewModel();
            //ultimate.category = "All";
            //ultimate.all = true;
            //return View(ultimate);
            #endregion

            var session = System.Web.HttpContext.Current.Session;
            int authenticateUserID = 0;
            if (SessionUtilities.GetAuthenticatedUserId() == 0)
            {
                session["CartItems"] = cartService.GetCartItems().Where(a => a.Status == "Pending" || a.Status == "NULL").Distinct().Count();
            }
            else
            {
                authenticateUserID = SessionUtilities.GetAuthenticatedUserId();
                var userDetail = userService.GetUserDataByUserID(authenticateUserID);
                session["CartItems"] = cartService.GetCartItemsByEmailID(userDetail.Email).Where(a => a.Status == "Pending" || a.Status == "NULL").Distinct().Count();
            }
                        
            session["HomePage"] = null;
                        
            List<ProductViewModel> listOfProductViewModels = new List<ProductViewModel>();
            UltimateViewModel ultimateViewModel = new UltimateViewModel();
            List<ProductDetails> productList = productService.GetAllProductDetail();

            foreach (var product in productList)
            {
                var model = new ProductViewModel();
                model.ProductID = product.ProductID;
                model.ProductName = product.ProductName;
                //model.CategoryName = productService.GetCategoryDetail(product.CategoryID).CategoryName;                
                if (product.Capacity != "0")
                    model.Capacity = product.Capacity;
                if (product.Chassis != "0")
                    model.Chassis = product.Chassis;
                if (product.Description != "0")
                    model.Description = product.Description;
                if (product.DimensionApprox != "0")
                    model.DimensionApprox = product.DimensionApprox;
                if (product.HppCylinders != "0")
                    model.HppCylinders = product.HppCylinders;
                if (product.Mobility != "0")
                    model.Mobility = product.Mobility;
                if (product.ModelNumber != "0")
                    model.ModelNumber = product.ModelNumber;
                if (product.PowerSource != "0")
                    model.PowerSource = product.PowerSource;
                if (product.UnitPrice != 0)
                    model.UnitPrice = product.UnitPrice;
                if (product.Weight != "0")
                    model.Weight = product.Weight;
                model.ImagePath = product.ImagePath;
                listOfProductViewModels.Add(model);
            }
            ultimateViewModel.ProductViewModelList = listOfProductViewModels;
            return View(ultimateViewModel);
        }

        #region Implement later
        //// SHow All Category List
        //public ActionResult AllCategoryList()
        //{
        //    UltimateViewModel ultimate = new UltimateViewModel();
        //    var categoryList = productService.findMainCategory();
        //    ultimate.ProductCategoryList = categoryList;
        //    return PartialView("_AllCategory", ultimate);
        //}

        //// To show Single Products when All Category is Selected
        //public ActionResult AllCategoryWiseListOfProduct()
        //{
        //    UltimateViewModel ultimate = new UltimateViewModel();
        //    var categoryList = productService.findMainCategory();
        //    List<ProductDetails> productDetails = new List<ProductDetails>();

        //    foreach (var item in categoryList)
        //    {
        //        var productFromEveryCatagory = productService.GetProductDetailByCategoryID(item.CategoryID);
        //        if(productFromEveryCatagory!= null)
        //        {
        //            productDetails.AddRange(productFromEveryCatagory);
        //        }                
        //    }


        //    #region Method for Cart List
        //    try
        //    {
        //        int currentUserID = SessionUtilities.GetAuthenticatedUserId();

        //        var cartMasterList = new List<CartMaster>();
        //        var cartDetailsList = new List<CartDetails>();


        //        var cartMaster = cartService.GetDataByUserId(currentUserID);                

        //        var cartDetailResult = cartService.GetDataByCartMasterId(cartMaster.CartMasterID);
        //        foreach (var cmList in cartDetailResult)
        //        {
        //            cartDetailsList.Add(cmList);
        //        }
        //        ultimate.CartDetails = cartDetailsList;
        //    }
        //    catch(Exception ex)
        //    {

        //    }


        //    #endregion


        //    ultimate.ProductDetails = productDetails;
        //    return PartialView("_AllCategoryWiseListOfProductView", ultimate);
        //}

        //public ActionResult ViewProductCategoryWise(int id)
        //{
        //    UltimateViewModel ultimate = new UltimateViewModel();

        //    List<ProductDetails> productList = new List<ProductDetails>();
        //    List<ProductsCategory> categoryList = productService.findMainCategory();
        //    ProductsCategory categoryDetail = productService.GetCategoryDetail(id);
        //    productList = productService.GetProductDetailByCategoryID(id);


        //    #region Method for Cart List
        //    try
        //    {
        //        int currentUserID = SessionUtilities.GetAuthenticatedUserId();

        //        var cartMasterList = new List<CartMaster>();
        //        var cartDetailsList = new List<CartDetails>();


        //        var cartMaster = cartService.GetDataByUserId(currentUserID);

        //        var cartDetailResult = cartService.GetDataByCartMasterId(cartMaster.CartMasterID);
        //        foreach (var cmList in cartDetailResult)
        //        {
        //            cartDetailsList.Add(cmList);
        //        }
        //        ultimate.CartDetails = cartDetailsList;
        //    }
        //    catch (Exception ex)
        //    {

        //    }


        //    #endregion

        //    ultimate.category = categoryDetail.CategoryName;
        //    ultimate.ProductDetails = productList;
        //    ultimate.ProductCategoryList = categoryList;
        //    return View(ultimate);
        //}
        #endregion


        public ActionResult ViewProduct(int productId)
        {
            UltimateViewModel ultimate = new UltimateViewModel();
            ultimate.productDetail = productService.GetSingleProductDetailByProductID(productId);
            
            #region Method for Cart List
            try
            {
                int currentUserID = SessionUtilities.GetAuthenticatedUserId();

                var cartMasterList = new List<CartMaster>();
                var cartDetailsList = new List<CartDetails>();


                var cartMaster = cartService.GetDataByUserId(currentUserID);

                var cartDetailResult = cartService.GetDataByCartMasterId(cartMaster.CartMasterID);
                foreach (var cmList in cartDetailResult)
                {
                    cartDetailsList.Add(cmList);
                }
                ultimate.CartDetails = cartDetailsList;
            }
            catch (Exception ex)
            {

            }
            #endregion

            return View(ultimate);
        }        
    }
}
