﻿//using System;
//using System.Collections.Generic;
//using System.Data;
//using System.Data.Entity;
//using System.Linq;
//using System.Net;
//using System.Web;
//using System.Web.Mvc;
//using DemoWebsite.Models;
//using System.Collections;
//using System.Threading.Tasks;
//using DemoWebsite.Services;

//namespace DemoWebsite.Controllers
//{
//    public class CategoryViewModelsController : Controller
//    {
//        private object db;

//        // GET: CategoryViewModels
//        public ActionResult Index()
//        {
//            List<CategoryViewModel> listOfCategoryViewModels = new List<CategoryViewModel>();
//            CategoryViewModelService service = new CategoryViewModelService();
//            listOfCategoryViewModels = service.GetListOfCategories();
//            return View(listOfCategoryViewModels);
//        }

//        //[HttpPost]
//        //[ValidateAntiForgeryToken]
//        //public ActionResult GetProducts(int id)
//        //{
//        //    ProductViewModel model = new ProductViewModel();

//        //    return View(ProductViewModel);
//        //}

//        // GET: CategoryViewModels/Details/5
//        //public ActionResult Details(int? id)
//        //{
//        //    if (id == null)
//        //    {
//        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//        //    }
//        //    CategoryViewModel categoryViewModel = db.Categories.Find(id);
//        //    if (categoryViewModel == null)
//        //    {
//        //        return HttpNotFound();
//        //    }
//        //    return View(categoryViewModel);
//        //}

//        // GET: CategoryViewModels/Create
//        public ActionResult Create()
//        {
//            return View();
//        }

//        // POST: CategoryViewModels/Create
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
//        //[HttpPost]
//        //[ValidateAntiForgeryToken]
//        //public ActionResult Create([Bind(Include = "CategoryID,CategoryName")] CategoryViewModel categoryViewModel)
//        //{
//        //    if (ModelState.IsValid)
//        //    {
//        //        db.CategoryViewModels.Add(categoryViewModel);
//        //        db.SaveChanges();
//        //        return RedirectToAction("Index");
//        //    }

//        //    return View(categoryViewModel);
//        //}

//        //// GET: CategoryViewModels/Edit/5
//        //public ActionResult Edit(int? id)
//        //{
//        //    if (id == null)
//        //    {
//        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//        //    }
//        //    CategoryViewModel categoryViewModel = db.CategoryViewModels.Find(id);
//        //    if (categoryViewModel == null)
//        //    {
//        //        return HttpNotFound();
//        //    }
//        //    return View(categoryViewModel);
//        //}

//        //// POST: CategoryViewModels/Edit/5
//        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
//        //[HttpPost]
//        //[ValidateAntiForgeryToken]
//        //public ActionResult Edit([Bind(Include = "CategoryID,CategoryName")] CategoryViewModel categoryViewModel)
//        //{
//        //    if (ModelState.IsValid)
//        //    {
//        //        db.Entry(categoryViewModel).State = EntityState.Modified;
//        //        db.SaveChanges();
//        //        return RedirectToAction("Index");
//        //    }
//        //    return View(categoryViewModel);
//        //}


//        //// GET: CategoryViewModels/Delete/5
//        //public ActionResult Delete(int? id)
//        //{
//        //    if (id == null)
//        //    {
//        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//        //    }
//        //    CategoryViewModel categoryViewModel = db.CategoryViewModels.Find(id);
//        //    if (categoryViewModel == null)
//        //    {
//        //        return HttpNotFound();
//        //    }
//        //    return View(categoryViewModel);
//        //}

//        //// POST: CategoryViewModels/Delete/5
//        //[HttpPost, ActionName("Delete")]
//        //[ValidateAntiForgeryToken]
//        //public ActionResult DeleteConfirmed(int id)
//        //{
//        //    CategoryViewModel categoryViewModel = db.CategoryViewModels.Find(id);
//        //    db.CategoryViewModels.Remove(categoryViewModel);
//        //    db.SaveChanges();
//        //    return RedirectToAction("Index");
//        //}

//        //protected override void Dispose(bool disposing)
//        //{
//        //    if (disposing)
//        //    {
//        //        db.Dispose();
//        //    }
//        //    base.Dispose(disposing);
//        //}
//    }
//}
