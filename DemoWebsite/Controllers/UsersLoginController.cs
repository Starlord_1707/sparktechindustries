﻿using Microsoft.AspNet.Identity;
using System;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using DemoWebsite.Services;
using DemoWebsite.Models;
using System.Linq;

namespace DemoWebsite.Controllers
{
    //[Authorize]
    public class UsersLoginController : Controller
    {
        #region Members
        //private SparkTechDB unitOfWork;

        private readonly IUserService userService;
        private readonly ICartService cartService;

        public UsersLoginController(IUserService user, ICartService cart)
        {
            userService = user;
            cartService = cart;
        }

        private UserManager<ApplicationUser> userManager { get; set; }
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        #endregion Members
        

        #region Actions
        // GET: /UsersLogin/Login
        
        [AllowAnonymous]
        public ActionResult Login()
        {
            Session["HomePage"] = null;
            //var vmLogin = new UserRegistration
            //{
            //    ReturnUrl = returnUrl
            //};

            //return View(vmLogin);
            return View();
        }

        // POST: /UsersLogin/Login
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(UserRegistration vmLogin)
        {
            #region Count the Cart Items
            //var session = System.Web.HttpContext.Current.Session;
            //int authenticateUserID = 0;
            //if (SessionUtilities.GetAuthenticatedUserId() == 0)
            //{
            //    session["CartItems"] = cartService.GetCartItems().Where(a => a.Status == "Pending" || a.Status == "NULL").Distinct().Count();
            //}
            //else
            //{
            //    authenticateUserID = SessionUtilities.GetAuthenticatedUserId();
            //    var userDetail = userService.GetUserDataByUserID(authenticateUserID);
            //    session["CartItems"] = cartService.GetCartItemsByEmailID(userDetail.Email).Where(a => a.Status == "Pending" || a.Status == "NULL").Distinct().Count();
            //}

            #endregion

            if (!ModelState.IsValid)
            {
                return View();
            }
            vmLogin.RememberMe = Convert.ToBoolean(Request.Form["rememberMe"]);
           
            var response = userService.CheckLogin(vmLogin);
            if (response.Response != null)
            {
                var identity = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.Sid, Convert.ToString(response.Response.UserId)),
                    new Claim(ClaimTypes.Name, response.Response.FirstName+" "+response.Response.LastName),
                    new Claim(ClaimTypes.Email, response.Response.Email),
                    new Claim(ClaimTypes.Role, response.Response.Role)
                }, "ApplicationCookie");

                var ctx = Request.GetOwinContext();
                var authManager = ctx.Authentication;
                authManager.SignIn(identity);
                if (vmLogin.RememberMe)
                {
                    HttpCookie cookie = new HttpCookie("iShopUserLogin");
                    cookie.Values.Add("userEmail", vmLogin.Email);
                    cookie.Expires = DateTime.Now.AddDays(15);
                    Response.Cookies.Add(cookie);
                }

                cartService.updateCartDataInDb(vmLogin.Email);
                //LoginHistoryService.InsertLoginHistory(Convert.ToString(response.Response.UserId), iShop.Models.CommonEnums.UserType.User);                
                return RedirectToAction("Index", "Home");
                
            }
            else
            {
                // user authN failed
                TempData["Error"] = "Invalid User ! Please try again with correct Email Id/Password";
                return View();
            }
            
        }
        private string GetRedirectUrl(string returnUrl)
        {
            if (string.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
            {
                return Url.Action("Index", "Home");
            }

            return returnUrl;
        }

        public ActionResult LogOut()
        {
            var ctx = Request.GetOwinContext();
            var authManager = ctx.Authentication;
            authManager.SignOut("ApplicationCookie");
            System.Web.HttpContext.Current.Session["CartId"] = System.Web.HttpContext.Current.Session.SessionID;
            //LoginHistoryService.UpdateLogoutTime(iShopHelper.GetCurrentUserId(), iShop.Models.CommonEnums.UserType.User);
            return RedirectToAction("Index", "Home");
        }

        #endregion Actions
        #region DataTable
        #endregion DataTable

        #region Methods
        #endregion Methods
    }
}