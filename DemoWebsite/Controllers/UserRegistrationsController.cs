﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Security.Claims;
using DemoWebsite.Models;
using DemoWebsite.Services;

namespace DemoWebsite.Controllers
{
    public class UserRegistrationsController : Controller
    {
        public IUserService userService; 
        public ICartService cartService;

        public UserRegistrationsController(IUserService user, ICartService cart)
        {
            userService = user;
            cartService = cart;
        }


        [HttpGet]
        public ActionResult Create()
        {
            Session["HomePage"] = null;
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(UserRegistration model)
        {
            #region Count the Cart Items

            //var session = System.Web.HttpContext.Current.Session;
            //int authenticateUserID = 0;
            //if (SessionUtilities.GetAuthenticatedUserId() == 0)
            //{
            //    session["CartItems"] = cartService.GetCartItems().Where(a => a.Status == "Pending" || a.Status == "NULL").Distinct().Count();
            //}
            //else
            //{
            //    authenticateUserID = SessionUtilities.GetAuthenticatedUserId();
            //    var userDetail = userService.GetUserDataByUserID(authenticateUserID);
            //    session["CartItems"] = cartService.GetCartItemsByEmailID(userDetail.Email).Where(a => a.Status == "Pending" || a.Status == "NULL").Distinct().Count();
            //}

            #endregion            



            if (ModelState.IsValid)
            {
                model.Role = "User";
                model.LastLogin = DateTime.Now;


                model.Status = "Active";       
                var updated = await userService.SaveData(model);
            if (updated.Exceptions.Any())
            {
                var excpetionMessage = updated.Exceptions.ToList();
                foreach (var ex in excpetionMessage)
                {
                    if (ex.Value == "First Name should not be Empty")
                    {
                        TempData["Error"] = ex.Value;
                        return View(model);
                    }
                    else if (ex.Value == "Last Name should not be Empty")
                    {
                        TempData["Error"] = ex.Value;
                        return View(model);
                    }
                    else if (ex.Key == "FieldSame")
                    {
                        TempData["Error"] = ex.Value;
                        return View(model);
                    }
                }
                    TempData["Error"] = "Email/Phone Number already exists";
                    return View(model);
            }
            if (updated.Response == false)
            {
                TempData["Error"] = "Email/Phone Number already exists";                    
                return View(model);
            }

            UserRegistration vmLogin = new UserRegistration();
            vmLogin.Email = model.Email;
            vmLogin.Password = model.Password;

            var response = userService.CheckLogin(vmLogin);
            if (response.Response != null)
            {
                var identity = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.Sid, Convert.ToString(response.Response.UserId)),
                    new Claim(ClaimTypes.Name, response.Response.FirstName+" "+response.Response.LastName),
                    new Claim(ClaimTypes.Email, response.Response.Email),
                    new Claim(ClaimTypes.Role, response.Response.Role),
                    new Claim(ClaimTypes.NameIdentifier, Convert.ToString(response.Response.UserId))
                    }, "ApplicationCookie");

                var ctx = Request.GetOwinContext();
                var authManager = ctx.Authentication;
                authManager.SignIn(identity);
                if (vmLogin.RememberMe)
                {
                    HttpCookie cookie = new HttpCookie("iShopUserLogin");
                    cookie.Values.Add("userEmail", vmLogin.Email);
                    cookie.Expires = DateTime.Now.AddDays(15);
                    Response.Cookies.Add(cookie);
                }
                    cartService.updateCartDataInDb(model.Email);
                    //LoginHistoryService.InsertLoginHistory(Convert.ToString(response.Response.UserId), iShop.Models.CommonEnums.UserType.User);
                    return Redirect(GetRedirectUrl(null));
                    
                }
               
            }
            else
           {
                    return Redirect(GetRedirectLoginUrl(null));
           }

           
            return View("Create", model);

    }
    private string GetRedirectUrl(string returnUrl)
    {
        if (string.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
        {
            return Url.Action("Index", "Home");
        }

        return returnUrl;
    }
        private string GetRedirectLoginUrl(string returnUrl)
        {
            if (string.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
            {
                return Url.Action("Login", "UsersLogin");
            }

            return returnUrl;
        }
    }
}