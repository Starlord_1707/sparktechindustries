﻿//using System;
//using System.Collections.Generic;
//using System.Data;
//using System.Data.Entity;
//using System.Linq;
//using System.Net;
//using System.Web;
//using System.Web.Mvc;
//using DemoWebsite.Models;

//namespace DemoWebsite.Controllers
//{
//    public class ProductViewModelsController : Controller
//    {
//        private AhmedMachineDataBaseEntities db = new AhmedMachineDataBaseEntities();

//        // GET: ProductViewModels
//        public ActionResult Index(int id)
//        {
//            //list of product view models for collecting data from database [Table - Products]
//            List<ProductViewModel> listOfProductViewModels = new List<ProductViewModel>();

//            foreach (Products product in db.Products.Where(product => product.CategoryID == id).ToList())
//            {
//                var model = new ProductViewModel();
//                model.ProductID = product.ProductID;
//                model.ProductName = product.ProductName;
//                model.CategoryName = product.Categories.CategoryName;
//                //var categoryName = db.Categories.Where(x => x.CategoryID == product.CategoryID).Select(y => y.CategoryName).ToArray();
//                ////model.Category.CategoryName = categoryName.ToString();
//                //model.Category.CategoryName = categoryName[0].ToString();
//                model.UnitPrice = product.UnitPrice;
//                model.Description = product.Description;
//                model.ImagePath = product.ImagePath;
//                listOfProductViewModels.Add(model);
//            }

//            //var productViewModels = db.ProductViewModels.Include(p => p.Category);
//            return View(listOfProductViewModels.ToList());
//        }

//        //GET: ProductViewModels/Details/5
//        //public ActionResult Details(int? id)
//        //{
//        //    if (id == null)
//        //    {
//        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//        //    }
//        //    ProductViewModel productViewModel = db.ProductViewModels.Find(id);
//        //    if (productViewModel == null)
//        //    {
//        //        return HttpNotFound();
//        //    }
//        //    return View(productViewModel);
//        //}

//        //public ActionResult Page1()
//        //{

//        //    return View();
//        //}

//        //// GET: ProductViewModels/Create
//        public ActionResult Create()
//        {
//            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "CategoryName");
//            return View();
//        }

//        // POST: ProductViewModels/Create
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create([Bind(Include = "ProductID,ProductName,Description,ImagePath,UnitPrice,CategoryID")] ProductViewModel productViewModel)
//        {
//            if (ModelState.IsValid)
//            {
//                var model = new Products();
//                model.CategoryID = productViewModel.CategoryID;
//                model.ProductID = productViewModel.ProductID;
//                model.Description = productViewModel.Description;
//                model.ImagePath = productViewModel.ImagePath;
//                model.UnitPrice = productViewModel.UnitPrice;
//                db.Products.Add(model);
//                db.SaveChanges();
//                return RedirectToAction("Index");
//            }

//            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "CategoryName", productViewModel.CategoryID);
//            return View(productViewModel);
//        }

//        // GET: ProductViewModels/Edit/5
//        //public ActionResult Edit(int? id)
//        //{
//        //    if (id == null)
//        //    {
//        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//        //    }
//        //    ProductViewModel productViewModel = db.ProductViewModels.Find(id);
//        //    if (productViewModel == null)
//        //    {
//        //        return HttpNotFound();
//        //    }
//        //    ViewBag.CategoryID = new SelectList(db.CategoryViewModels, "CategoryID", "CategoryName", productViewModel.CategoryID);
//        //    return View(productViewModel);
//        //}

//        // POST: ProductViewModels/Edit/5
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        //public ActionResult Edit([Bind(Include = "ProductID,ProductName,Description,ImagePath,UnitPrice,CategoryID")] ProductViewModel productViewModel)
//        //{
//        //    if (ModelState.IsValid)
//        //    {
//        //        db.Entry(productViewModel).State = EntityState.Modified;
//        //        db.SaveChanges();
//        //        return RedirectToAction("Index");
//        //    }
//        //    ViewBag.CategoryID = new SelectList(db.CategoryViewModels, "CategoryID", "CategoryName", productViewModel.CategoryID);
//        //    return View(productViewModel);
//        //}

//        // GET: ProductViewModels/Delete/5
//        //public ActionResult Delete(int? id)
//        //{
//        //    if (id == null)
//        //    {
//        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//        //    }
//        //    ProductViewModel productViewModel = db.ProductViewModels.Find(id);
//        //    if (productViewModel == null)
//        //    {
//        //        return HttpNotFound();
//        //    }
//        //    return View(productViewModel);
//        //}

//        // POST: ProductViewModels/Delete/5
//        //[HttpPost, ActionName("Delete")]
//        //[ValidateAntiForgeryToken]
//        //public ActionResult DeleteConfirmed(int id)
//        //{
//        //    ProductViewModel productViewModel = db.ProductViewModels.Find(id);
//        //    db.ProductViewModels.Remove(productViewModel);
//        //    db.SaveChanges();
//        //    return RedirectToAction("Index");
//        //}

//        protected override void Dispose(bool disposing)
//        {
//            if (disposing)
//            {
//                db.Dispose();
//            }
//            base.Dispose(disposing);
//        }
//    }
//}
