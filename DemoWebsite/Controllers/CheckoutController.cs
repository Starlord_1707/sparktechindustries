﻿using DemoWebsite.Models;
using DemoWebsite.Models.DBContext;
using DemoWebsite.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace DemoWebsite.Controllers
{
    public class CheckoutController : Controller
    {
        private readonly IUserService userService;
        private readonly ICartService cartService;
        private readonly IOrderService orderService;
        private readonly IProductsService productService;

        public CheckoutController(IUserService userProd, ICartService cartServ, IProductsService productServ, IOrderService orderServ)
        {
            userService = userProd;
            cartService = cartServ;
            productService = productServ;
            orderService = orderServ;
        }

        // GET: Products
        public ActionResult Index()
        {
            int currentID = SessionUtilities.GetAuthenticatedUserId();
            Session["HomePage"] = null;
            var showModel = userService.GetUserDataByUserID(currentID);
            return View(showModel);

        }

        [HttpPost]
        public ActionResult AddressDetails(UserRegistration vmUsersRegistration)
        {

            int currentID = SessionUtilities.GetAuthenticatedUserId();
            var editModel = userService.GetUserDataByUserID(currentID);
            vmUsersRegistration.UserId = currentID;
            vmUsersRegistration.FirstName = editModel.FirstName;

            TempData["Street"] = Request.Form["shipStreet"];
            TempData["City"] = Request.Form["shipCity"];
            TempData["State"] = Request.Form["shipState"];
            TempData["Country"] = Request.Form["shipCountry"];
            TempData["Pincode"] = Request.Form["shipPin"];

            vmUsersRegistration.State = Request.Form["shipState"];

            var updated = userService.SaveDataDuringCheckout(vmUsersRegistration);
            if (updated.Response == false )
            {
                return Json(new { status = false, message = "false" });
            }
            else if (updated.Response == false)
            {
                TempData["Error"] = "Data already exists.";
                return View(vmUsersRegistration);
            }
            // var model = await userProfileManager.GetDataById(currentID);
            return RedirectToAction("Confirmation", "Checkout");
        }

        
        public ActionResult Confirmation()
        {

            UltimateViewModel ultimateModel = new UltimateViewModel();

            #region Confirm and Save the Order

            List<ShoppingCartItems> cartDetailList = new List<ShoppingCartItems>();

            ShoppingCartItems shoppingCart = new ShoppingCartItems();
            
            Orders vmOrders = new Orders();

            CartMaster vmCartMaster = new CartMaster();

            
            int currentID = SessionUtilities.GetAuthenticatedUserId();
            var userDetail = userService.GetUserDataByUserID(currentID);

            


            cartDetailList = cartService.GetCartItemsByEmailID(userDetail.Email).Where(a=> a.Status=="Pending" || a.Status == "NULL").ToList();
            double orderPrice = 0;
            foreach (var cartPrice in cartDetailList)
            {
                var prodDetail = productService.GetSingleProductDetailByProductID(cartPrice.ProductId);
                orderPrice = orderPrice + cartPrice.Quantity * prodDetail.UnitPrice??0;
            }


            

            vmOrders.UserID = currentID;
            vmOrders.OrderAmount = Convert.ToDecimal(orderPrice+5000);
            var date = DateTime.Now;
            var onlyDate = date.ToString("yyyyMMddHHmmss");
            var orderno = "#" + onlyDate + "_" + currentID;
            vmOrders.OrderNumber = orderno;
            vmOrders.OrderDate = DateTime.Now;

            // Need to Improvise for date of delivery
            vmOrders.DeliveredDate = date.AddDays(5);
            vmOrders.Status = "Processing";
            var updatedOrder = orderService.SaveOrder(vmOrders);
            if (updatedOrder)
            {
               
                    var orderResult = orderService.GetDataByUserIdAndOrderNumber(currentID, vmOrders.OrderNumber);                                       
                    if(orderResult==null)
                    {
                        TempData["error"] = "Please try Again Later";
                        return View();  
                    }
                   
                                    
                    foreach (var orderDetail in cartDetailList)
                    {
                        OrderDetails vmOrderDetails = new OrderDetails();
                        if (orderDetail.Status != "Delete")
                        {

                            vmOrderDetails.Status = "Pending";
                            vmOrderDetails.OrderID = orderResult.OrderID;
                            vmOrderDetails.ProductId = orderDetail.ProductId;
                            vmOrderDetails.Quantity = orderDetail.Quantity;
                            var updatedOrderDetails = orderService.SaveOrderDetails(vmOrderDetails);
                        }
                    }



                #region Update Cartmaster
                vmCartMaster.UserID = currentID;               
                vmCartMaster.OrderID = orderResult.OrderID;
                vmCartMaster.OrderPrice = vmOrders.OrderAmount;
               
                var updatedCartMaster = cartService.SaveCartMasterData(vmCartMaster);
                #endregion

                #region Update Shopping Cart Items                
                List<ShoppingCartItems> shoppingUpdate = cartService.GetCartItemsByEmailID(userDetail.Email);
                foreach(var itemUpdate in shoppingUpdate)
                {
                    itemUpdate.Status = "Completed";
                    cartService.UpdateShoppingCart(itemUpdate);
                }
                #endregion


                // Write code here to send email     
                #region For Email
                //try
                //{


                //    var tempUserData = userDetail;

                //    var Address = tempUserData.FirstName + " " + tempUserData.LastName + ", " + tempUserData.Street + ", " + tempUserData.City + ", " + tempUserData.State + ", " + tempUserData.Country + ", " + tempUserData.PinCode;

                //    var ShiipingAddress = Address;
                //    var BillingAddress = Address;

                //    var tempOrderDetails = orderService.GetData();
                //    var tempOrderList = "";

                //    var tempproductDetails = await productDetailsManager.GetData();


                //    foreach (var temp in tempOrderDetails.Response)
                //    {
                //        if (temp.OrderID == orderResult.Response.OrderID)
                //        {
                //            foreach (var tempProduct in tempproductDetails.Response)
                //            {
                //                if (tempProduct.ProductID == temp.ProductId)
                //                {
                //                    var productQuantityPrice = Convert.ToInt32(vmOrderDetails.Quantity) * Convert.ToInt32(tempProduct.Price);
                //                    tempOrderList += "<tr><td>&nbsp;</td><td align='center'></td><td style='text-align:center'>" + tempProduct.ProductName + "</td><td style='text-align:center'>" + temp.Quantity + "</td><td style='text-align:center'>Rs " + tempProduct.Price + "</td><td style='text-align:center;width:25%'>Rs " + productQuantityPrice + "</td></tr> ";
                //                }

                //            }
                //        }
                //    }


                //    var OrderItems = tempOrderList;




                //    var body = createEmailBody(vmOrders.OrderNumber, Convert.ToString(vmOrders.OrderDate), Convert.ToString(vmOrders.OrderAmount), OrderItems, BillingAddress, ShiipingAddress);

                //    var mailAddress = tempUserData.Response.Email;

                //    var subject = "Invoice for order number :" + vmOrders.OrderNumber;

                //    await SendEmail.SendMailAsync(subject, body, mailAddress);
                //    TempData["CurrentOrderId"] = vmOrders.OrderNumber; //To display single order after payment




                    //return RedirectToAction("Index");
                //}
                //catch (Exception ex)
                //{
                //    return RedirectToAction("Index", "Home");
                //}
                #endregion



            }
            else 
            {
                TempData["error"] = "Please try Again Later";
                return View();
            }
                       
            #endregion


            #region Show the Product Details
            

            // Get User Detail           
           
            ultimateModel.userRegistration = userDetail;

            // Get Cart Details

            var productList = new List<ProductDetails>();
            var productCartView = new List<ProductViewForCart>();


            // Get here the All the orders of the Current Cart

            var recentOrder = orderService.GetDataByUserIdAndOrderNumber(currentID,vmOrders.OrderNumber);


            // Get Order Detail 

            var recentOrderDetail = orderService.GetOrderDetailDataByOrderId(recentOrder.OrderID);
            if (recentOrderDetail != null)
            {
                foreach (var prod in recentOrderDetail)
                {
                    // Added the Product Details of that Product in the Cart
                    var prodDetail = productService.GetSingleProductDetailByProductID(prod.ProductId);
                    productList.Add(prodDetail);
                    ProductViewForCart prodCartObject = new ProductViewForCart();
                    // For SubTotal
                    prodCartObject.ProductID = prod.ProductId;
                    prodCartObject.ProductQty = prod.Quantity;
                    prodCartObject.SubTotal = prodDetail.UnitPrice ?? 0 * prod.Quantity;
                    productCartView.Add(prodCartObject);
                }
            }
            
            var sum = productCartView.Sum(a => a.SubTotal);
            TempData["subTotalForAllProduct"] = sum;
            TempData["subTotalPlus"] = sum + 5000;


           
            Session["CartItems"] = cartService.GetCartItemsByEmailID(userDetail.Email).Where(a => a.Status == "Pending" || a.Status == "NULL").Distinct().Count();
           


            #endregion


            #region Show Order Details
            TempData["orderNo"] = orderno;

            #endregion


            ultimateModel.ProductDetails = productList;
            ultimateModel.shoppingCartItems = cartDetailList;
            ultimateModel.productViewForCart = productCartView;

            return View(ultimateModel);

        }
    }
}
