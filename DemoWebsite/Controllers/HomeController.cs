﻿using DemoWebsite.Models;
using DemoWebsite.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DemoWebsite.Controllers
{
    public class HomeController : BaseController
    {
        private readonly ICartService cartService;
        private readonly IUserService userService;

        public HomeController(ICartService cartServ, IUserService userServ)
        {
            cartService = cartServ;
            userService = userServ;
        }
        public ActionResult Index()
        {

            //Apply this In EveryPage where session is used
            var session = System.Web.HttpContext.Current.Session;
            int authenticateUserID = 0;
            if (SessionUtilities.GetAuthenticatedUserId()==0)
            {
                session["CartItems"] = cartService.GetCartItems().Where(a => a.Status == "Pending" || a.Status == "NULL").Distinct().Count();
            }
            else
            {
                authenticateUserID = SessionUtilities.GetAuthenticatedUserId();
                var userDetail = userService.GetUserDataByUserID(authenticateUserID);
                session["CartItems"] = cartService.GetCartItemsByEmailID(userDetail.Email).Where(a => a.Status == "Pending" || a.Status == "NULL").Distinct().Count();
            }
            session["HomePage"] = 1;
            return View();
        }
        
        public ActionResult countCartItems()
        {
            int currentUserID = SessionUtilities.GetAuthenticatedUserId();
            var cartResult = cartService.GetDataByUserId(currentUserID);

            if (cartResult != null)
            {
                if (cartResult.UserID == currentUserID)
                {
                    int cartMasterId = cartResult.CartMasterID;
                    int cartDetailCount = cartService.CountbyCartMasterId(cartMasterId);
                    
                    TempData["TotalCartItems"] = cartDetailCount==0?"0":cartDetailCount.ToString();
                }
            }
            return PartialView("_countCartItems");

        }

        public ActionResult NavigationBar()
        {
            

            return PartialView("_navigationBar");
        }
    }
}