﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DemoWebsite.Models;
using System.Net.Mail;
using System.IO;
using DemoWebsite.Services;
using static DemoWebsite.Services.EmailTriggerService;

namespace DemoWebsite.Controllers
{
    public class EmailController : Controller
    {
        EmailTriggerService service = new EmailTriggerService();
        // GET: Email
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// send an email to support time - feedback from user
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="userEmail"></param>
        /// <param name="emailBodyContent"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SendEmail(string userName, string userEmail, string emailBodyContent)
        {
            var message = string.Empty;
            string toEmail = "akshtiwari7@hotmail.com";
            EmailBodyType emailBodyType = EmailBodyType.MessageFromUser;
            message = service.SendEmailDataContent(userName, userEmail, toEmail, emailBodyContent, emailBodyType);
            //service.SendEmailToUser(toEmail, emailBody, emailSubject, emailBodyIsHtml);
            var jsonResult = Json(message, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        /// <summary>
        /// send an OTP to verify user credentials
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="userEmail"></param>
        /// <param name="emailBodyContent"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SendOTPEmail(string userName, string userEmail)
        {
            var message = string.Empty;
            string toEmail = userEmail;
            EmailBodyType emailBodyType = EmailBodyType.VerifyCredentials;
            string emailBodyContent = string.Empty;
            Random oneTimePassword = new Random();
            int oneTimePassowrdToSend = oneTimePassword.Next(123456, 897645);
            emailBodyContent = oneTimePassowrdToSend.ToString();
            var session = System.Web.HttpContext.Current.Session;
            session["OneTimePassword"] = oneTimePassowrdToSend;
            message = service.SendEmailDataContent(userName, userEmail, toEmail, emailBodyContent, emailBodyType);
            //service.SendEmailToUser(toEmail, emailBody, emailSubject, emailBodyIsHtml);
            var jsonResult = Json(message, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        [HttpPost]
        public ActionResult VerifyOneTimePassword(string userName, string userEmail, int oneTimePassword)
        {
            bool flag = false;
            string toEmail = userEmail;
            var session = System.Web.HttpContext.Current.Session;
            string otpFromUser = oneTimePassword.ToString();
            if (!string.IsNullOrEmpty(otpFromUser))
            {
                if (session["OneTimePassword"].ToString() == otpFromUser)
                    flag = true;
                else
                    flag = false;
            }
            var jsonResult = Json(flag, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        [HttpPost]
        public ActionResult VerifyCredentials(string userName, string userEmail)
        {
            var message = string.Empty;
            string toEmail = userEmail;
            EmailBodyType emailBodyType = EmailBodyType.VerifyCredentials;
            string emailBodyContent = string.Empty;
            Random oneTimePassword = new Random();
            int oneTimePassowrdToSend = oneTimePassword.Next(123456, 897645);
            emailBodyContent = oneTimePassowrdToSend.ToString();
            var session = System.Web.HttpContext.Current.Session;
            session["OneTimePassword"] = oneTimePassowrdToSend;
            message = service.SendEmailDataContent(userName, userEmail, toEmail, emailBodyContent, emailBodyType);
            //service.SendEmailToUser(toEmail, emailBody, emailSubject, emailBodyIsHtml);
            var jsonResult = Json(message, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }


    }
}