﻿//using DemoWebsite.Models;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;
//namespace DemoWebsite.Controllers
//{
//    public class ShoppingCartController : Controller
//    {
//        // GET: ShoppingCart
//        public ActionResult Index()
//        {
//            return View();
//        }

//        public string ShoppingCartId { get; set; }

//        private AhmedMachineDataBaseEntities db = new AhmedMachineDataBaseEntities();

//        public const string CartSessionKey = "CartId";

//        [HttpPost]
//        public ActionResult AddToCart(int id)
//        {
//            // Retrieve the product from the database.  
//            ShoppingCartId = GetCartId();

//            var cartItem = db.ShoppingCartItems.SingleOrDefault(
//                c => c.CartId == ShoppingCartId
//                && c.ProductId == id);
//            if (cartItem == null)
//            {
//                // Create a new cart item if no cart item exists.                 
//                cartItem = new ShoppingCartItems
//                {
//                    ItemId = Guid.NewGuid().ToString(),
//                    ProductId = id,
//                    CartId = ShoppingCartId,
//                    Products = db.Products.SingleOrDefault(
//                   p => p.ProductID == id),
//                    Quantity = 1,
//                    DateCreated = DateTime.Now
//                };
//                db.ShoppingCartItems.Add(cartItem);
//            }
//            else
//            {
//                // If the item does exist in the cart,                  
//                // then add one to the quantity.                 
//                cartItem.Quantity++;
//            }
//            db.SaveChanges();
//            System.Web.HttpContext.Current.Session["CartItems"] = GetCartItems().Distinct().Count();
//            var cartItemsForView = System.Web.HttpContext.Current.Session["CartItems"];
//            var jsonResult = Json(cartItemsForView, JsonRequestBehavior.AllowGet);
//            jsonResult.MaxJsonLength = int.MaxValue;
//            return jsonResult;
//        }

//        public ActionResult _CartItemsOnHeader()
//        {
//            return View();
//        }

//        public void Dispose()
//        {
//            if (db != null)
//            {
//                db.Dispose();
//                db = null;
//            }
//        }

//        public string GetCartId()
//        {
//            if (System.Web.HttpContext.Current.Session[CartSessionKey] == null)
//            {
//                if (!string.IsNullOrWhiteSpace(System.Web.HttpContext.Current.User.Identity.Name))
//                {
//                    System.Web.HttpContext.Current.Session[CartSessionKey] = System.Web.HttpContext.Current.User.Identity.Name;
//                }
//                else
//                {
//                    // Generate a new random GUID using System.Guid class.     
//                    //Guid tempCartId = Guid.NewGuid();
//                    //System.Web.HttpContext.Current.Session[CartSessionKey] = tempCartId.ToString();
//                    System.Web.HttpContext.Current.Session[CartSessionKey] = System.Web.HttpContext.Current.Session.SessionID;
//                }
//            }
//            return System.Web.HttpContext.Current.Session[CartSessionKey].ToString();
//        }

//        public List<ShoppingCartItems> GetCartItems()
//        {
//            ShoppingCartId = GetCartId();

//            return db.ShoppingCartItems.Where(c => c.CartId == ShoppingCartId).ToList();
//        }

//        //public decimal GetTotal()
//        //{
//        //    ShoppingCartId = GetCartId();
//        //    // Multiply product price by quantity of that product to get        
//        //    // the current price for each of those products in the cart.  
//        //    // Sum all product price totals to get the cart total.   
//        //    decimal? total = decimal.Zero;
//        //    total = (decimal?)(from cartItems in _db.ShoppingCartItems
//        //                       where cartItems.CartId == ShoppingCartId
//        //                       select (int?)cartItems.Quantity *
//        //                       cartItems.Product.UnitPrice).Sum();
//        //    return total ?? decimal.Zero;
//        //}

//        //public ShoppingCartActions GetCart(HttpContext context)
//        //{
//        //    using (var cart = new ShoppingCartActions())
//        //    {
//        //        cart.ShoppingCartId = cart.GetCartId();
//        //        return cart;
//        //    }
//        //}

//        //public void UpdateShoppingCartDatabase(String cartId, ShoppingCartUpdates[] CartItemUpdates)
//        //{
//        //    using (var db = new WingtipToys.Models.ProductContext())
//        //    {
//        //        try
//        //        {
//        //            int CartItemCount = CartItemUpdates.Count();
//        //            List<CartItem> myCart = GetCartItems();
//        //            foreach (var cartItem in myCart)
//        //            {
//        //                // Iterate through all rows within shopping cart list
//        //                for (int i = 0; i < CartItemCount; i++)
//        //                {
//        //                    if (cartItem.Product.ProductID == CartItemUpdates[i].ProductId)
//        //                    {
//        //                        if (CartItemUpdates[i].PurchaseQuantity < 1 || CartItemUpdates[i].RemoveItem == true)
//        //                        {
//        //                            RemoveItem(cartId, cartItem.ProductId);
//        //                        }
//        //                        else
//        //                        {
//        //                            UpdateItem(cartId, cartItem.ProductId, CartItemUpdates[i].PurchaseQuantity);
//        //                        }
//        //                    }
//        //                }
//        //            }
//        //        }
//        //        catch (Exception exp)
//        //        {
//        //            throw new Exception("ERROR: Unable to Update Cart Database - " + exp.Message.ToString(), exp);
//        //        }
//        //    }
//        //}

//        //public void RemoveItem(string removeCartID, int removeProductID)
//        //{
//        //    using (var _db = new WingtipToys.Models.ProductContext())
//        //    {
//        //        try
//        //        {
//        //            var myItem = (from c in _db.ShoppingCartItems where c.CartId == removeCartID && c.Product.ProductID == removeProductID select c).FirstOrDefault();
//        //            if (myItem != null)
//        //            {
//        //                // Remove Item.
//        //                _db.ShoppingCartItems.Remove(myItem);
//        //                _db.SaveChanges();
//        //            }
//        //        }
//        //        catch (Exception exp)
//        //        {
//        //            throw new Exception("ERROR: Unable to Remove Cart Item - " + exp.Message.ToString(), exp);
//        //        }
//        //    }
//        //}

//        //public void UpdateItem(string updateCartID, int updateProductID, int quantity)
//        //{
//        //    using (var _db = new WingtipToys.Models.ProductContext())
//        //    {
//        //        try
//        //        {
//        //            var myItem = (from c in _db.ShoppingCartItems where c.CartId == updateCartID && c.Product.ProductID == updateProductID select c).FirstOrDefault();
//        //            if (myItem != null)
//        //            {
//        //                myItem.Quantity = quantity;
//        //                _db.SaveChanges();
//        //            }
//        //        }
//        //        catch (Exception exp)
//        //        {
//        //            throw new Exception("ERROR: Unable to Update Cart Item - " + exp.Message.ToString(), exp);
//        //        }
//        //    }
//        //}

//        //public void EmptyCart()
//        //{
//        //    ShoppingCartId = GetCartId();
//        //    var cartItems = _db.ShoppingCartItems.Where(
//        //        c => c.CartId == ShoppingCartId);
//        //    foreach (var cartItem in cartItems)
//        //    {
//        //        _db.ShoppingCartItems.Remove(cartItem);
//        //    }
//        //    // Save changes.             
//        //    _db.SaveChanges();
//        //}

//        //public int GetCount()
//        //{
//        //    ShoppingCartId = GetCartId();

//        //    // Get the count of each item in the cart and sum them up          
//        //    int? count = (from cartItems in _db.ShoppingCartItems
//        //                  where cartItems.CartId == ShoppingCartId
//        //                  select (int?)cartItems.Quantity).Sum();
//        //    // Return 0 if all entries are null         
//        //    return count ?? 0;
//        //}

//        public struct ShoppingCartUpdates
//        {
//            public int ProductId;
//            public int PurchaseQuantity;
//            public bool RemoveItem;
//        }

//        // GET: ShoppingCart/Details/5
//        public ActionResult Details(int id)
//        {
//            return View();
//        }

//        // GET: ShoppingCart/Create
//        public ActionResult Create()
//        {
//            return View();
//        }

//        // POST: ShoppingCart/Create
//        [HttpPost]
//        public ActionResult Create(FormCollection collection)
//        {
//            try
//            {
//                // TODO: Add insert logic here

//                return RedirectToAction("Index");
//            }
//            catch
//            {
//                return View();
//            }
//        }

//        // GET: ShoppingCart/Edit/5
//        public ActionResult Edit(int id)
//        {
//            return View();
//        }

//        // POST: ShoppingCart/Edit/5
//        [HttpPost]
//        public ActionResult Edit(int id, FormCollection collection)
//        {
//            try
//            {
//                // TODO: Add update logic here

//                return RedirectToAction("Index");
//            }
//            catch
//            {
//                return View();
//            }
//        }

//        // GET: ShoppingCart/Delete/5
//        public ActionResult Delete(int id)
//        {
//            return View();
//        }

//        // POST: ShoppingCart/Delete/5
//        [HttpPost]
//        public ActionResult Delete(int id, FormCollection collection)
//        {
//            try
//            {
//                // TODO: Add delete logic here

//                return RedirectToAction("Index");
//            }
//            catch
//            {
//                return View();
//            }
//        }
//    }
//}
