﻿using DemoWebsite.Models;
using DemoWebsite.Models.DBContext;
using DemoWebsite.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace DemoWebsite.Controllers
{
    public class CartController : Controller
    {
        private readonly ICartService cartService;
        private readonly IProductsService productService;
        private readonly IUserService userService;
        public string ShoppingCartId { get; set; }
        public CartController(ICartService cartServ, IProductsService prodServ, IUserService userServ)
        {
            cartService = cartServ;
            productService = prodServ;
            userService = userServ;
        }

        // GET: Products
        public ActionResult Index()
        {
            Session["HomePage"] = null;
            int authenticateUserID = 0;
            if (SessionUtilities.GetAuthenticatedUserId() == 0)
            {
                Session["CartItems"] = cartService.GetCartItems().Where(a => a.Status == "Pending" || a.Status == "NULL").Distinct().Count();
            }
            else
            {
                authenticateUserID = SessionUtilities.GetAuthenticatedUserId();
                var userDetail = userService.GetUserDataByUserID(authenticateUserID);
                Session["CartItems"] = cartService.GetCartItemsByEmailID(userDetail.Email).Where(a => a.Status == "Pending" || a.Status == "NULL").Distinct().Count();
            }
            return View();
        }

        public ActionResult AfterLogin()
        {

            var productList = new List<ProductDetails>();           
            var cartDetailList = new List<CartDetails>();
            var productViewForCartList = new List<ProductViewForCart>();
            var productView = new ProductViewForCart();

            var productCategoryList = new List<ProductsCategory>();

            int currentUserID = SessionUtilities.GetAuthenticatedUserId();
            var cartMasterResult = cartService.GetDataByUserId(currentUserID);

            if (cartMasterResult == null)
            {
                UltimateViewModel tempmodel = new UltimateViewModel();
                tempmodel.ProductDetails = null;
                return PartialView("_CheckoutLogin", tempmodel);
            }
            var cartDetailResult = cartService.GetDataByCartMasterId(cartMasterResult.CartMasterID);            
                
            foreach (var cdList in cartDetailResult)
            {
                 var productDetailResult = productService.GetSingleProductDetailByProductID(cdList.ProductID);
                 
                 if (productDetailResult.ProductID == cdList.ProductID && cdList.Status != "Delete")
                 {
                    productView.ProductID = productDetailResult.ProductID;
                    productView.ProductQty = cdList.ProductQty;

                    productViewForCartList.Add(productView);
                    productList.Add(productDetailResult);
                 }                                 
            }            

            foreach (var cdList in cartDetailResult)
            {
                cartDetailList.Add(cdList);
            }

            int cartDetailCount = cartService.CountbyCartMasterId(cartMasterResult.CartMasterID);
            if (cartDetailCount == 0)
            {
                UltimateViewModel  tempmodel = new UltimateViewModel();
                tempmodel.ProductDetails = null;
                return PartialView("_CartLogin", tempmodel);
            }



            TempData["subTotal"] = cartMasterResult.OrderPrice;
            TempData["subTotalPlus"] = cartMasterResult.OrderPrice + 5000;

            //var model = productList;

            var tempProductCategoryList = productService.findMainCategory();
            foreach (var piList in tempProductCategoryList)
            {
                productCategoryList.Add(piList);
            }


            UltimateViewModel model = new UltimateViewModel();
            model.ProductDetails = productList;            
            model.ProductCategoryList = productCategoryList;
            model.CartDetails = cartDetailList;
            model.productViewForCart = productViewForCartList;

            return PartialView("_CartLogin", model);
           
        }

        public ActionResult NotLogin()
        {
            var cartProductIds = string.Empty;
            try
            {
                cartProductIds = Request.Cookies["proID"].Value;
                if (cartProductIds == "0" || cartProductIds == "")
                {
                    UltimateViewModel tempmodel1 = new UltimateViewModel();
                    tempmodel1.ProductDetails = null;
                    return PartialView("_CartNotLogin", tempmodel1);
                }
                else
                {
                    cartProductIds = HttpUtility.UrlDecode(cartProductIds ?? string.Empty);
                    var allProductCartId = cartProductIds.Split(',').Select(x => x.Trim()).ToArray();
                    var productList = new List<ProductDetails>();

                    for (int i = 0; i < allProductCartId.Length; i++)
                    {
                        if (allProductCartId[i] != null)
                        {
                            var productIDTemp = Convert.ToInt32(allProductCartId[i]);
                            var t = productList.FirstOrDefault(x => x.ProductID == productIDTemp);
                            if (t == null)
                            {
                                var tempProductList = productService.GetSingleProductDetailByProductID(productIDTemp);
                                productList.Add(tempProductList);
                            }

                        }
                    }

                    UltimateViewModel model = new UltimateViewModel();

                    var productCategoryList = new List<ProductsCategory>();
                    //var model = productList;

                    var tempProductCategoryList = productService.findMainCategory();
                    foreach (var piList in tempProductCategoryList)
                    {
                        productCategoryList.Add(piList);
                    }
                    model.ProductDetails = productList;
                    model.ProductCategoryList = productCategoryList;


                    return PartialView("_CartNotLogin", model);
                }
                //return PartialView("_CartNotLogin");
            }
            catch
            {
                UltimateViewModel tempmodel = new UltimateViewModel();
                tempmodel.ProductDetails = null;
                return PartialView("_CartNotLogin", tempmodel);
            }

        }


        #region Add To Cart Later
        //[HttpPost]
        //public JsonResult AddToCart(int TheProductId)
        //{
        //    //if user is logged in
        //    CartDetails vmCartDetails = new CartDetails();
        //    CartMaster vmCartMaster = new CartMaster();
        //    int cartResultForCount;
        //    int currentUserID = SessionUtilities.GetAuthenticatedUserId();

        //    vmCartMaster.UserID = currentUserID;

        //    var cartResultBefore = cartService.GetDataByUserId(currentUserID);
        //    decimal? price = 0;
        //    if (cartResultBefore != null)
        //    {
        //        if (cartResultBefore.UserID == currentUserID)
        //        {

        //            #region Checking if this product is already added then raise its quantity
        //            vmCartDetails.ProductQty = 1;
        //            var cartDetailResult = cartService.GetDataByCartMasterId(cartResultBefore.CartMasterID);
        //            if(cartDetailResult!=null)
        //            {
        //                foreach(var item in cartDetailResult)
        //                {
        //                    if(item.ProductID==TheProductId)
        //                    {
        //                        vmCartDetails.ProductQty = item.ProductQty + 1;
        //                    }                           
        //                }                        

        //            }
        //            else
        //            {
        //                vmCartDetails.ProductQty = 1;
        //            }

        //            vmCartDetails.CartMasterID = cartResultBefore.CartMasterID;
        //            vmCartDetails.ProductID = TheProductId;
        //            var productResult = productService.GetSingleProductDetailByProductID(TheProductId);
        //            vmCartDetails.UnitPrice = Convert.ToDecimal(productResult.UnitPrice);

        //            if (cartResultBefore.OrderPrice == null)
        //            {
        //                price = vmCartDetails.UnitPrice + 0;
        //            }
        //            else
        //            {
        //                price = vmCartDetails.UnitPrice + cartResultBefore.OrderPrice;
        //            }

        //            vmCartMaster.OrderPrice = price;
        //            vmCartMaster.CartMasterID = cartResultBefore.CartMasterID;


        //            #endregion



        //            var updateCartMaster = cartService.SaveCartMasterData(vmCartMaster);                    
        //            var updatedCartDetails = cartService.SaveCartDetailsData(vmCartDetails);
        //            if (updatedCartDetails == false)
        //            {
        //                return Json(new { status = false, message = "false" });
        //            }
        //            else if (updatedCartDetails == true)
        //            {
        //                //return Json(vmCartDetails, JsonRequestBehavior.AllowGet);
        //               cartResultForCount = cartService.CountbyCartMasterId(vmCartMaster.CartMasterID);
        //                return Json(cartResultForCount, JsonRequestBehavior.AllowGet);
        //            }

        //        }

        //    } // if the cartmasterID already exist with Userid and status Pending

        //    else if (cartResultBefore == null)
        //    {
        //        var productResultForOrderPrice = productService.GetSingleProductDetailByProductID(TheProductId);
        //        vmCartMaster.OrderPrice = Convert.ToDecimal(productResultForOrderPrice.UnitPrice);
        //        var updatedCartMaster = cartService.SaveCartMasterData(vmCartMaster);
        //        if (updatedCartMaster)
        //        {

        //                var cartResult = cartService.GetDataByUserId(currentUserID);
        //                if (cartResult!=null)
        //                {
        //                    return Json(new { status = false, message = "false" });
        //                }
        //                vmCartDetails.CartMasterID = cartResult.CartMasterID;
        //                vmCartDetails.ProductID = TheProductId;

        //                var productResult = productService.GetSingleProductDetailByProductID(TheProductId);
        //                vmCartDetails.UnitPrice = Convert.ToDecimal(productResult.UnitPrice);                        
        //                vmCartDetails.ProductQty = 1;
        //                var updatedCartDetails = cartService.SaveCartDetailsData(vmCartDetails);
        //                if (updatedCartDetails == false)
        //                {
        //                    return Json(new { status = false, message = "false" });
        //                }
        //                else if (updatedCartDetails == true)
        //                {
        //                //return Json(vmCartDetails, JsonRequestBehavior.AllowGet);
        //                cartResultForCount = cartService.CountbyCartMasterId(vmCartMaster.CartMasterID);
        //                return Json(cartResultForCount, JsonRequestBehavior.AllowGet);
        //            }

        //        }
        //        else if (updatedCartMaster == false)
        //        {
        //            return Json(vmCartMaster, JsonRequestBehavior.AllowGet);

        //        }
        //    }

        //    cartResultForCount = cartService.CountbyCartMasterId(cartResultBefore.CartMasterID);
        //    return Json(cartResultForCount, JsonRequestBehavior.AllowGet);
        //}

        #endregion

        [HttpPost]
        public ActionResult AddToCart(int id)
        {
            // Retrieve the product from the database.
            int authenticateUserID = 0;
            if (SessionUtilities.GetAuthenticatedUserId() == 0)
            {
                ShoppingCartId = cartService.GetCartId();
            }
            else
            {
                authenticateUserID = SessionUtilities.GetAuthenticatedUserId();
                var userDetail = userService.GetUserDataByUserID(authenticateUserID);
                ShoppingCartId = userDetail.Email;
            }
            var cartItem = cartService.GetSingleCartItems(ShoppingCartId, id);
            if (cartItem == null)
            {
                // Create a new cart item if no cart item exists.                 
                cartItem = new ShoppingCartItems
                {
                    ItemId = Guid.NewGuid().ToString(),
                    ProductId = id,
                    CartId = ShoppingCartId,                    
                    Quantity = 1,
                    DateCreated = DateTime.Now,
                    Status = "Pending"
                };
                cartService.SaveShoppingCart(cartItem);

            }
            else
            {   // If the item does exist in the cart,                  
                // then add one to the quantity.                      
                cartItem.Quantity++;
                cartService.UpdateShoppingCart(cartItem);
            }
            int cartItemsForView = 0;
            if (SessionUtilities.GetAuthenticatedUserId() == 0)
            {
                cartItemsForView = cartService.GetCartItems().Where(a => a.Status == "Pending" || a.Status == "NULL").Distinct().Count();
            }
            else
            {
                authenticateUserID = SessionUtilities.GetAuthenticatedUserId();
                var userDetail = userService.GetUserDataByUserID(authenticateUserID);
                cartItemsForView = cartService.GetCartItemsByEmailID(userDetail.Email).Where(a => a.Status == "Pending" || a.Status == "NULL").Distinct().Count();
            }

             
            var jsonResult = Json(cartItemsForView, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public JsonResult DeleteProduct(int id)
        {
            if (id == 0)
            {
                return Json(new { status = false, message = "false" });
            }
            int currentUserID = SessionUtilities.GetAuthenticatedUserId();
            var cartMasterResult = cartService.GetDataByUserId(currentUserID);
            var cartDetailResult = cartService.GetDataByCartMasterId(cartMasterResult.CartMasterID);
            var detailID = 0;

            foreach (var temp in cartDetailResult)
            {
                if (temp.ProductID == id)
                {
                    detailID = temp.CartDetailID;
                    break;
                }
            }

            var activity = cartService.GetCartDetailDataById(detailID);

            CartDetails model = activity;

            string statusval = "Delete";
            model.Status = statusval;
            
            decimal? price = 0;

            CartMaster vmCartMaster = new CartMaster();
            vmCartMaster.UserID = currentUserID;
            
            vmCartMaster.CartMasterID = cartMasterResult.CartMasterID;

            var cartResultCount = cartService.GetDataByUserId(currentUserID);
            int cartDetailCountForPrice = cartService.CountbyCartMasterId(cartResultCount.CartMasterID);
            if (cartDetailCountForPrice == 2)
            {
                var cartDetailsResultForDelete = cartService.GetDataByCartMasterId(cartResultCount.CartMasterID);
                foreach (var deleteDetail in cartDetailsResultForDelete)
                {
                    if (deleteDetail.CartDetailID != detailID)
                    {
                        price = (deleteDetail.ProductQty) * (deleteDetail.UnitPrice);
                    }
                }

            }
            else
            {
                price = cartMasterResult.OrderPrice - ((model.ProductQty) * (model.UnitPrice));
            }

            vmCartMaster.OrderPrice = price;

            var updateCartMaster = cartService.SaveCartMasterData(vmCartMaster);
            var updated = cartService.SaveCartDetailsData(model);


            var cartResultForCount = cartService.GetDataByUserId(currentUserID);
            int cartMasterId = cartResultForCount.CartMasterID;
            int cartDetailCount = cartService.CountbyCartMasterId(cartMasterId);
            var orderPrice = cartResultForCount.OrderPrice;
            var includeShipPrice = cartResultForCount.OrderPrice + 5000;
            var result = new { Count = cartDetailCount, Subtotal = orderPrice , SubtotalPlus = includeShipPrice};

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult EmptyCart()
        {
            return null ;
        }


        public ActionResult ViewCart()
        {
            var productList = new List<ProductDetails>();            
            var productCartView = new List<ProductViewForCart>();



            List<ShoppingCartItems> cartDetailList = new List<ShoppingCartItems>();

            // Get the Cart Details of the Current Session ID
            int authenticateUserID = 0;
            if (SessionUtilities.GetAuthenticatedUserId() == 0)
            {
                cartDetailList = cartService.GetCartItems().Where(a => a.Status == "Pending" || a.Status == "NULL").Distinct().ToList();
            }
            else
            {
                authenticateUserID = SessionUtilities.GetAuthenticatedUserId();
                var userDetail = userService.GetUserDataByUserID(authenticateUserID);
                cartDetailList = cartService.GetCartItemsByEmailID(userDetail.Email).Where(a => a.Status == "Pending" || a.Status == "NULL").Distinct().ToList();
            }            
            if(cartDetailList!=null)
            {
                foreach (var prod in cartDetailList.Where(a=> a.Status=="Pending" || a.Status==""))
                {
                    // Added the Product Details of that Product in the Cart
                    var prodDetail = productService.GetSingleProductDetailByProductID(prod.ProductId);
                    productList.Add(prodDetail);
                    ProductViewForCart prodCartObject = new ProductViewForCart();
                    // For SubTotal
                    prodCartObject.ProductID = prod.ProductId;
                    prodCartObject.ProductQty = prod.Quantity;
                    prodCartObject.SubTotal = (prodDetail.UnitPrice??0) * prod.Quantity;
                    productCartView.Add(prodCartObject);
                }
            }
            var sum = productCartView.Sum(a => a.SubTotal);
            TempData["subTotalForAllProduct"] = sum;
            TempData["subTotalPlus"] = sum + 5000;
            UltimateViewModel model = new UltimateViewModel();
            model.ProductDetails = productList;
            model.shoppingCartItems = cartDetailList;
            model.productViewForCart = productCartView;
            return PartialView("_ViewCart", model);
        }

        public JsonResult DeleteProductFromCart(int id)
        {
            double includeShipPrice = 0;
            int cartDetailCount =0;
            if (id == 0)
            {
                return Json(new { status = false, message = "false" });
            }
            bool response;
            int authenticateUserID = 0;
            if (SessionUtilities.GetAuthenticatedUserId() == 0)
            {                
                response = cartService.DeleteCartItemUsingProductId(id, cartService.GetCartId());
            }
            else
            {
                authenticateUserID = SessionUtilities.GetAuthenticatedUserId();
                var userDetail = userService.GetUserDataByUserID(authenticateUserID);
                response = cartService.DeleteCartItemUsingProductId(id,userDetail.Email);
            }
            
            if(response)
            {
                ProductViewForCart prodCartObject = new ProductViewForCart();
                List<ProductViewForCart> productCartView = new List<ProductViewForCart>();
                List<ShoppingCartItems> cartDetailList = cartService.GetCartItems();
                if (cartDetailList != null)
                {
                    foreach (var prod in cartDetailList.Where(a => a.Status == "Pending" || a.Status == ""))
                    {
                        var prodDetail = productService.GetSingleProductDetailByProductID(prod.ProductId);
                        // For SubTotal
                        prodCartObject.ProductID = prod.ProductId;
                        prodCartObject.ProductQty = prod.Quantity;
                        prodCartObject.SubTotal = prodDetail.UnitPrice ?? 0 * prod.Quantity;
                        productCartView.Add(prodCartObject);
                    }
                }
                var sum = productCartView.Sum(a => a.SubTotal);
                includeShipPrice = sum + 5000;

                if (SessionUtilities.GetAuthenticatedUserId() == 0)
                {
                    cartDetailCount = cartService.GetCartItems().Where(a => a.Status == "Pending" || a.Status == "NULL").Distinct().Count();
                }
                else
                {
                    authenticateUserID = SessionUtilities.GetAuthenticatedUserId();
                    var userDetail = userService.GetUserDataByUserID(authenticateUserID);
                    cartDetailCount = cartService.GetCartItemsByEmailID(userDetail.Email).Where(a => a.Status == "Pending" || a.Status == "NULL").Distinct().Count();
                }
                var result = new { Count = cartDetailCount, Subtotal = sum, SubtotalPlus = includeShipPrice };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { status = false, message = "Please try Again" });
            }            
        }
    }
}
