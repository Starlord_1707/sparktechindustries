using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoWebsite.Models
{
    public class ContactUs 
    {
        public int Id { get; set; }

        [MaxLength(150)]
        [Required]
        public string Name { get; set; }
        [MaxLength(150)]
        [Required]
        [EmailAddress(ErrorMessage ="Please enter valid Email")]
        public string Email { get; set; }
        [MaxLength(2500)]
        [Required]
        [DataType(DataType.MultilineText)]
        public string Message { get; set; }

        [Display(Name="Created On")]
        public DateTime CreatedDate { get; set; } = DateTime.Now;

        public CommonEnums.Status Status { get; set; } = CommonEnums.Status.Active;

    }
}
