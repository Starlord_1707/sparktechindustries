﻿using System.ComponentModel.DataAnnotations;

namespace DemoWebsite.Models
{
    public class CartMaster
    {
        [Key]
        public int CartMasterID { get; set; }
        public int UserID { get; set; }
        public string Status { get; set; }
        public int? OrderID { get; set; }

        public decimal? OrderPrice { get; set; }


    }
}
