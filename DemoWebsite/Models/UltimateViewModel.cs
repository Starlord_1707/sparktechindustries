﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoWebsite.Models
{
    public class UltimateViewModel
    {
            public string category { get; set; }
            public bool? all { get; set; }
            public IEnumerable<ProductDetails> ProductDetails { get; set; }            
            public IEnumerable<ProductsCategory> ProductCategoryList { get; set; }
            public IEnumerable<CartDetails> CartDetails { get; set; }
            public ProductDetails productDetail { get; set; }
            public IEnumerable<ProductViewForCart> productViewForCart { get; set; }
            public IEnumerable<ProductViewModel> ProductViewModelList { get; set; }
            public IEnumerable<ShoppingCartItems> shoppingCartItems { get; set; }
            public IEnumerable<OrderDetailsVM> orderDetailView { get; set; }
            public UserRegistration userRegistration { get; set; }

    }
}