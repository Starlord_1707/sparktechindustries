﻿using System.ComponentModel.DataAnnotations;

namespace DemoWebsite.Models
{
    public class ProductsCategory
    {
        [Key]
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
        public int ParentID { get; set; }
        public string Status { get; set; }
    }
}