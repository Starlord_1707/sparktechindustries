﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoWebsite.Models
{
    public class ProductViewForCart
    {
        public int ProductID { get; set; }       
        public int ProductQty { get; set; }
        public double SubTotal { get; set; }
    }
}