﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoWebsite.Models
{
   public class OrderDetailsVM
    {
        public string OrderNo { get; set; }
        public List<OrderDetails> orderDetails { get; set; }
    }
}
