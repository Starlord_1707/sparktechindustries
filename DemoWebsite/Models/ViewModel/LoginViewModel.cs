﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoWebsite.Models
{
   public class LoginViewModel
    {
        [Key]
        public int UserId { get; set; } 
               
        [Required(ErrorMessage = "Email ID shouldn't be Empty")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Enter valid Email Address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Enter Password.")]        
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
     

    }
}
