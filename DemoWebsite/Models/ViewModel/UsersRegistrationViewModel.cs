﻿using System;
using System.ComponentModel.DataAnnotations;


namespace DemoWebsite.Models
{
   public class UsersRegistrationViewModel
    {
        [Key]
        public int UserId { get; set; }
        //[Required(ErrorMessage = "Field shouldn't be Empty")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        //[Required(ErrorMessage = "Field shouldn't be Empty")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Email ID shouldn't be Empty")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Enter valid Email Address")]
        public string Email { get; set; }

        //[Required(ErrorMessage = "Field shouldn't be Empty.")]
        [MaxLength(10, ErrorMessage = "Phone number should be upto 10 digit")]
        [MinLength(10, ErrorMessage = "Phone number should be upto 10 digit")]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
        [Required(ErrorMessage = "Choose Password.")]
        [MaxLength(10, ErrorMessage = "Password Should not be greater than 10 Characters")]
        [MinLength(8, ErrorMessage = "Password Should not be less than 8 Characters")]
        //[RegularExpression(@"^.*(?=.*[!@#$%^&*\(\)_\-+=]).*$",ErrorMessage = "Password Not Strong Enough")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }


        //[Required(ErrorMessage = "Enter Confirm Password.")]
        //[Compare(nameof(Password), ErrorMessage = "Passwords doesn't match.")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        public string ConfirmPassword { get; set; }
        public string DOB { get; set; }
        public string Gender { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PinCode { get; set; }
        public string Role { get; set; }
        public string Status { get; set; }
        public DateTime? LastLogin { get; set; }
        public bool RememberMe { get; set; }

        [ScaffoldColumn(false)]
        public string ReturnUrl { get; set; }
    }
}
