﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoWebsite.Models
{
    public class Feedback 
    {
        public int Id { get; set; }
        public int ProductID { get; set; }
        public int UserID { get; set; }
        public string Email { get; set; }
        public string Content { get; set; }
        public string Status { get; set; }

    }
}
