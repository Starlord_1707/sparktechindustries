﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoWebsite.Models
{
  public class UserRegistration
    {

        // Change when run migration
        [Key]
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
              
        public string PhoneNumber { get; set; }
               
        public string Password { get; set; }
        public string DOB { get; set; }
        public string Gender { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PinCode { get; set; }
        public string Role { get; set; }
        public string Status { get; set; }
        public bool RememberMe { get; set; }
        public DateTime? LastLogin { get; set; }        

    }
}

