using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoWebsite.Models
{
    public class CommonEnums
    {
        public enum SystemSettingType
        {
            Admin = 1,
            SuperAdmin = 2,

        }

        public enum UserType
        {

            Admin = 1,
            SuperAdmin = 2,
            User = 3
        }

        public enum ClaimType
        {

            Create = 1,
            Edit = 2,
            Delete = 3,
            View = 4
        }

        public enum ClaimTypeValue
        {
            Allow=1,
            Deny=2
        }

        public enum Status
        {
            Active = 1,
            Deactive = 2,
            Deleted = 3
        }
        public enum OperationType
        {
            Read = 1,
            Write = 2

        }
        public enum ResponseCodes
        {
            Error = 1,
            Success = 2,
            NoDataFound = 3
        }

    }

    public sealed class CryptoUtilities
    {
        #region Class Members
        public const string Select = "Select";
        public const string Active = "Active";
        public const string Deactive = "Deactive";
        public const string Delete = "Delete";
        #endregion
    }
}
