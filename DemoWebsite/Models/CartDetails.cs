﻿using System.ComponentModel.DataAnnotations;


namespace DemoWebsite.Models
{
    public class CartDetails
    {
        [Key]
        public int CartDetailID { get; set; }
        public int CartMasterID { get; set; }
        public int ProductID { get; set; }
        public string ProductColor { get; set; }
        public string ProductSize { get; set; }
        public decimal UnitPrice { get; set; }
        public int ProductQty { get; set; }
        public string Status { get; set; }
       


    }
}
