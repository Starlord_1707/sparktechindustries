﻿using System.ComponentModel.DataAnnotations;

namespace DemoWebsite.Models
{
    public class OrderDetails 
    {
        [Key]
        public int OrderDetailsID { get; set; }
        public int OrderID { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public string Status { get; set; }


    }


}
