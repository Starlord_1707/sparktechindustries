﻿using System.ComponentModel.DataAnnotations;

namespace DemoWebsite.Models
{
    public class Login 
    {
        #region Properties
        [Key]
        /// <summary>
        /// There are no comments for email in the schema.
        /// </summary>
        public virtual string emailID
        {
            get;
            set;
        }


        /// <summary>
        /// There are no comments for password in the schema.
        /// </summary>
        public virtual string password
        {
            get;
            set;
        }


        /// <summary>
        /// There are no comments for guid in the schema.
        /// </summary>
        public virtual string guid
        {
            get;
            set;
        }
        #endregion
    }
}
