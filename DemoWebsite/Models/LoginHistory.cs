
using System;


namespace DemoWebsite.Models
{
    public class LoginHistory
    {
        public int Id { get; set; }

        public string  UserId { get; set; }
        public DateTime LoginTime { get; set; }

        public DateTime? LogoutTime { get; set; }

        public string UserIPAddress { get; set; }
        
        public string UserOS { get; set; }

        public CommonEnums.UserType UserType { get; set; }

    }
}
