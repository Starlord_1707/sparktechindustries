﻿using System.ComponentModel.DataAnnotations;

namespace DemoWebsite.Models
{
    public class ProductImages
    {
        [Key]
        public int imageID { get; set; }
        public int productID { get; set; }
        public string productThumbnail { get; set; }
        public string productFullSize { get; set; }
        public string imageDescription { get; set; }
        public string Status { get; set; }
    }
}