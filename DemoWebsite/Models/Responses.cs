﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoWebsite.Models
{
    public class Responses<T>
    {
        public T Response
        {
            get;
            set;
        }
        public Dictionary<string, string> Exceptions
        {
            get;
            set;
        }
        public int TotalRecords
        {
            get;
            set;
        }
        public decimal RecordID
        {
            get;
            set;
        }
    }
}