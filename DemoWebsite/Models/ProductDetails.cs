﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DemoWebsite.Models
{
    public class ProductDetails
    {
        [Key]
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public string ImagePath { get; set; }
        public string ModelNumber { get; set; }
        public string Capacity { get; set; }
        public string Weight { get; set; }
        public string DimensionApprox { get; set; }
        public string PowerSource { get; set; }
        public string Chassis { get; set; }
        public string Mobility { get; set; }
        public string HppCylinders { get; set; }
        public string Availability { get; set; }
        public Nullable<double> UnitPrice { get; set; }
        public int CategoryID { get; set; }

    }
}