﻿using System.Data.Entity;


namespace DemoWebsite.Models.DBContext
{
    public class SparkTechDBContext : DbContext
    {

        public SparkTechDBContext() :  base("name=SparkTechContext")
        {

        }

        #region DbSet
        public DbSet<Login> Logins { get; set; }
        public DbSet<UserRegistration> UserRegistrations { get; set; }       
        public DbSet<CartDetails> CartDetails { get; set; }
        public DbSet<CartMaster> CartMasters { get; set; }
        public DbSet<ContactUs> ContactUs { get; set; }        
        public DbSet<Feedback> Feedbacks { get; set; }
        public DbSet<LoginHistory> LoginHistory { get; set; }
        public DbSet<OrderDetails> OrderDetails { get; set; }
        public DbSet<Orders> Orders { get; set; }
        public DbSet<ProductsCategory> ProductsCategory { get; set; }
        public DbSet<ProductDetails> ProductDetails { get; set; }
        public DbSet<ProductImages> ProductImages { get; set; }
        public DbSet<ShoppingCartItems> ShoppingCartItems { get; set; }
        
        #endregion

    }
}
