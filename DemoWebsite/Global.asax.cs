﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Data.Entity;
using DemoWebsite.Models;


namespace DemoWebsite
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            UnityConfig.RegisterComponents();
            System.Web.Helpers.AntiForgeryConfig.UniqueClaimTypeIdentifier =
             System.Security.Claims.ClaimTypes.Email;
            //// Initialize the product database.
            //Database.SetInitializer(new ProductDatabaseInitializer());
        }
    }
}
